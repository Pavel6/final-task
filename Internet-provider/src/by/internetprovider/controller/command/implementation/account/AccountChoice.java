package by.internetprovider.controller.command.implementation.account;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.Account;
import by.internetprovider.controller.command.Command;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to represent a customer with a choice from his accounts.
 * 
 * @author Pavel Pranovich
 *
 */
public class AccountChoice implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		HttpSession session = request.getSession(true);
		String login = (String) session.getAttribute("login");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		AccountService accountServiceImpl = serviceFactory.getAccountServiceImpl();
		Set<Account> clientAccounts = accountServiceImpl.getClientAccounts(login);

		session.setAttribute("userAction", "showAccountChoice");
		session.setAttribute("clientAccounts", clientAccounts);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
