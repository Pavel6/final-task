package by.internetprovider.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Class for an account of client.
 * 
 * <p>
 * One client can have one or more accounts.
 * 
 * @author Pavel Pranovich
 *
 */

public class Account implements Comparable<Account>, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * User (client) of the account.
	 */
	private User user;

	/**
	 * Account number.
	 */
	private byte accountNumber;

	/**
	 * Balance (amount of payment means).
	 */
	private float balance;

	/**
	 * Unused traffic volume.
	 */
	private int unusedTraffic;

	/**
	 * Status of ban on the account.
	 */
	private boolean banStatus;

	/**
	 * Name of the current tariff plan.
	 */
	private String tariffName;

	/**
	 * Start date of the billing period of the current tariff plan.
	 */
	private Date tariffStartDate;

	/**
	 * Expiry date of the billing period of the current tariff plan.
	 */
	private Date tariffEndDate;

	/**
	 * Creates a new instance of client's account.
	 */
	public Account() {
		super();
	}

	/**
	 * Creates a new instance of client's account for the specified
	 * <code>{@link User}</code>, <code>balance</code>,
	 * <code>accountNumber</code>, <code>banStatus</code>,
	 * <code>tariffName</code>, <code>tariffStartDate</code>,
	 * <code>tariffEndDate</code>.
	 * 
	 * @param user
	 *            - user of the account.
	 * @param balance
	 *            - balance (amount of means of payment).
	 * @param accountNumber
	 *            - account number.
	 * @param banStatus
	 *            - status of ban.
	 * @param tariffName
	 *            - tariff plan name.
	 * @param tariffStartDate
	 *            - start date of the billing period of tariff plan.
	 * @param tariffEndDate
	 *            - expiry date of the billing period of tariff plan.
	 */
	public Account(User user, float balance, byte accountNumber, boolean banStatus, String tariffName,
			Date tariffStartDate, Date tariffEndDate) {
		super();
		this.user = user;
		this.balance = balance;
		this.accountNumber = accountNumber;
		this.banStatus = banStatus;
		this.tariffName = tariffName;
		this.tariffStartDate = tariffStartDate;
		this.tariffEndDate = tariffEndDate;
	}

	/**
	 * Creates a new instance of client's account for the specified
	 * <code>accountNumber</code>, <code>balance</code>,
	 * <code>unusedTraffic</code>, <code>banStatus</code>,
	 * <code>tariffName</code>, <code>tariffStartDate</code>,
	 * <code>tariffEndDate</code>.
	 * 
	 * @param accountNumber
	 *            - account number.
	 * @param balance
	 *            - balance (amount of means of payment).
	 * @param unusedTraffic
	 *            - unused traffic volume.
	 * @param banStatus
	 *            - status of ban.
	 * @param tariffName
	 *            - tariff plan name.
	 * @param tariffStartDate
	 *            - tart date of the billing period of tariff plan.
	 * @param tariffEndDate
	 *            - expiry date of the billing period of tariff plan.
	 */
	public Account(byte accountNumber, float balance, int unusedTraffic, boolean banStatus, String tariffName,
			Date tariffStartDate, Date tariffEndDate) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.unusedTraffic = unusedTraffic;
		this.banStatus = banStatus;
		this.tariffName = tariffName;
		this.tariffStartDate = tariffStartDate;
		this.tariffEndDate = tariffEndDate;
	}

	/**
	 * Creates a new instance of client's account for the specified
	 * <code>{@link User}</code>, <code>accountNumber</code>,
	 * <code>tariffName</code>.
	 * 
	 * @param user
	 *            - user of the account.
	 * @param accountNumber
	 *            - account number.
	 * @param tariffName
	 *            - tariff plan name.
	 */
	public Account(User user, byte accountNumber, String tariffName) {
		super();
		this.user = user;
		this.accountNumber = accountNumber;
		this.tariffName = tariffName;
	}

	public User getUser() {
		return user;
	}

	public byte getAccountNumber() {
		return accountNumber;
	}

	public float getBalance() {
		return balance;
	}

	public int getUnusedTraffic() {
		return unusedTraffic;
	}

	public boolean isBanStatus() {
		return banStatus;
	}

	public String getTariffName() {
		return tariffName;
	}

	public Date getTariffStartDate() {
		return tariffStartDate;
	}

	public Date getTariffEndDate() {
		return tariffEndDate;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setAccountNumber(byte accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public void setUnusedTraffic(int unusedTraffic) {
		this.unusedTraffic = unusedTraffic;
	}

	public void setBanStatus(boolean banStatus) {
		this.banStatus = banStatus;
	}

	public void setTariffName(String tariffName) {
		this.tariffName = tariffName;
	}

	public void setTariffStartDate(Date tariffStartDate) {
		this.tariffStartDate = tariffStartDate;
	}

	public void setTariffEndDate(Date tariffEndDate) {
		this.tariffEndDate = tariffEndDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountNumber;
		result = prime * result + Float.floatToIntBits(balance);
		result = prime * result + (banStatus ? 1231 : 1237);
		result = prime * result + ((tariffEndDate == null) ? 0 : tariffEndDate.hashCode());
		result = prime * result + ((tariffName == null) ? 0 : tariffName.hashCode());
		result = prime * result + ((tariffStartDate == null) ? 0 : tariffStartDate.hashCode());
		result = prime * result + unusedTraffic;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (Float.floatToIntBits(balance) != Float.floatToIntBits(other.balance))
			return false;
		if (banStatus != other.banStatus)
			return false;
		if (tariffEndDate == null) {
			if (other.tariffEndDate != null)
				return false;
		} else if (!tariffEndDate.equals(other.tariffEndDate))
			return false;
		if (tariffName == null) {
			if (other.tariffName != null)
				return false;
		} else if (!tariffName.equals(other.tariffName))
			return false;
		if (tariffStartDate == null) {
			if (other.tariffStartDate != null)
				return false;
		} else if (!tariffStartDate.equals(other.tariffStartDate))
			return false;
		if (unusedTraffic != other.unusedTraffic)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [user=" + user + ", accountNumber=" + accountNumber + ", balance=" + balance
				+ ", unusedTraffic=" + unusedTraffic + ", banStatus=" + banStatus + ", tariffName=" + tariffName
				+ ", tariffStartDate=" + tariffStartDate + ", tariffEndDate=" + tariffEndDate + "]";
	}

	@Override
	public int compareTo(Account otherAcc) {

		if (this.user.compareTo(otherAcc.user) == 0) {
			if (this.accountNumber == otherAcc.accountNumber) {
				return this.tariffName.compareTo(otherAcc.tariffName);

			} else {
				return this.accountNumber - otherAcc.accountNumber;

			}
		} else {
			return this.user.compareTo(otherAcc.user);

		}
	}

}
