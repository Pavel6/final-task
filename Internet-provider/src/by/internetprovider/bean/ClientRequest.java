package by.internetprovider.bean;

import java.io.Serializable;

/**
 * Class for a request of client for tariff plan activation.
 * 
 * @author Pavel Pranovich
 *
 */
public class ClientRequest implements Comparable<ClientRequest>, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Login of the client for which request is created.
	 */
	private String clientLogin;

	/**
	 * Account number of the client for which request is created.
	 */
	private byte accountNumber;

	/**
	 * Name of the tariff plan chosen by client.
	 */
	private String tariffName;

	/**
	 * Creates a new client request.
	 */
	public ClientRequest() {
		super();
	}

	/**
	 * Creates a new client request for the specified <code>clientLogin</code>,
	 * <code>accountNumber</code> and <code>tariffName</code>.
	 * 
	 * @param clientLogin
	 *            - Login of the client
	 * @param accountNumber
	 *            - client account number
	 * @param tariffName
	 *            - tariff plan name
	 */
	public ClientRequest(String clientLogin, byte accountNumber, String tariffName) {
		super();
		this.clientLogin = clientLogin;
		this.accountNumber = accountNumber;
		this.tariffName = tariffName;
	}

	public String getUserLogin() {
		return clientLogin;
	}

	public byte getAccountNumber() {
		return accountNumber;
	}

	public String getTariffName() {
		return tariffName;
	}

	public void setUserLogin(String userLogin) {
		this.clientLogin = userLogin;
	}

	public void setAccountNumber(byte accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setTariffName(String tariffName) {
		this.tariffName = tariffName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountNumber;
		result = prime * result + ((tariffName == null) ? 0 : tariffName.hashCode());
		result = prime * result + ((clientLogin == null) ? 0 : clientLogin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientRequest other = (ClientRequest) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (tariffName == null) {
			if (other.tariffName != null)
				return false;
		} else if (!tariffName.equals(other.tariffName))
			return false;
		if (clientLogin == null) {
			if (other.clientLogin != null)
				return false;
		} else if (!clientLogin.equals(other.clientLogin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserRequest [userLogin=" + clientLogin + ", accountNumber=" + accountNumber + ", tariffName="
				+ tariffName + "]";
	}

	@Override
	public int compareTo(ClientRequest otherUserReq) {

		if (this.clientLogin.compareTo(otherUserReq.clientLogin) == 0) {
			if (this.accountNumber == otherUserReq.accountNumber) {
				return this.tariffName.compareTo(otherUserReq.tariffName);

			} else {
				return this.accountNumber - otherUserReq.accountNumber;

			}
		} else {
			return this.clientLogin.compareTo(otherUserReq.clientLogin);

		}

	}
}
