package by.internetprovider.controller.command.implementation.user;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.RegFormCheckout;
import by.internetprovider.bean.RegUser;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.localesupport.LocaleMessageSupport;
import by.internetprovider.service.UserService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for registering a new client in the system.
 * 
 * @author Pavel Pranovich
 *
 */
public class ClientRegistration implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, IOException, NamingException {

		RegUser newClient = initialiseRegUser(request);

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		UserService userServiceIml = serviceFactory.getUserServiceImpl();
		RegFormCheckout formCheckout = userServiceIml.registrateClient(newClient);

		redirectToPage(request, response, formCheckout);

	}

	/*
	 * Auxiliary method for selecting the redirect page depending on the state
	 * of the RegFormCheckout object.
	 */
	private static void redirectToPage(HttpServletRequest request, HttpServletResponse response,
			RegFormCheckout formCheckout) throws IOException {

		HttpSession session = request.getSession(true);

		if (formCheckout.isValidationPassed()) {
			if (formCheckout.isLoginFree()) {
				redirect(response, session, "page", "/page/successregistration.jsp");

			} else {
				session.setAttribute("regError",
						LocaleMessageSupport.getLocalizedMessage(request, "local.incorrectlogin"));
				redirect(response, session, "page", "/page/registration.jsp");
			}
		} else {
			session.setAttribute("regError", LocaleMessageSupport.getLocalizedMessage(request, "local.incorrectenter"));
			redirect(response, session, "page", "/page/registration.jsp");
		}
	}

	/*
	 * Auxiliary method for initializing the RegUser object for a new client.
	 */
	private static RegUser initialiseRegUser(HttpServletRequest request) {

		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String login = request.getParameter("login");
		String password = request.getParameter("password1");
		byte age = Byte.parseByte(request.getParameter("age"));
		String eMail = request.getParameter("mail");
		String eMailAlt = request.getParameter("mailAlt");

		RegUser newClient = new RegUser(name, surname, login, password, age, eMail, eMailAlt);

		return newClient;
	}

	/*
	 * Auxiliary method for redirecting.
	 */
	private static void redirect(HttpServletResponse response, HttpSession session, String attrName, String attrValue)
			throws IOException {

		session.setAttribute(attrName, attrValue);

		response.sendRedirect("InternetProviderController?command=goto_page");
	}

}
