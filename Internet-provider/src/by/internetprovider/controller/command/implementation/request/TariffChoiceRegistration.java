package by.internetprovider.controller.command.implementation.request;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.command.implementation.tariff.TariffViewAndChoice;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to register a client request for changing a tariff plan.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffChoiceRegistration implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException, NamingException {

		String login = request.getParameter("login");
		byte userAccountNumber = Byte.parseByte(request.getParameter("accountnumber"));
		String chosenTariffName = request.getParameter("tariffname");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		RequestService requestServiceImpl = serviceFactory.getRequestServiceImpl();
		String answerMessage = requestServiceImpl.registerClientRequest(login, userAccountNumber, chosenTariffName);

		HttpSession session = request.getSession(true);
		session.setAttribute("tariffRegAnswer", answerMessage);

		TariffViewAndChoice tariffChoice = new TariffViewAndChoice();
		tariffChoice.execute(request, response);

	}

}
