<%@ page contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.internetprovider.localization.local"
	var="loc" />
	
<fmt:message key="local.emptyfield" bundle="${loc}" var="emptyfield" />
<fmt:message key="local.wrongfirstsymbol" bundle="${loc}" var="wrongfirstsymbol" />
<fmt:message key="local.notenoughsymbols" bundle="${loc}" var="notenoughsymbols" />
<fmt:message key="local.incorrectpass" bundle="${loc}" var="incorrectpass" />
<fmt:message key="local.firstsymbol" bundle="${loc}" var="firstsymbol" />
<fmt:message key="local.loginsymbols" bundle="${loc}" var="loginsymbols" />
<fmt:message key="local.passsymbols" bundle="${loc}" var="passsymbols" />
<fmt:message key="local.login" bundle="${loc}" var="login" />
<fmt:message key="local.password" bundle="${loc}" var="password" />

function validateSignInForm() {
	var result = true; 
	
	// for alert about wrong fields
	var alertMessage = "";
	
	var EMPTY_FIELD = " ${emptyfield}",
	    WRONG_FIRST_SYMBOL = " ${wrongfirstsymbol}",
	    WRONG_SYMBOL_QUANT = " ${notenoughsymbols}",
	    WRONG_PSWD = " ${incorrectpass}";
	
	var errLogin = document.getElementById("errLoginEnter"),
    	errPassword = document.getElementById("errPassEnter");
	
	errLogin.innerHTML = "";
    errPassword.innerHTML = "";
	
	var login = document.forms["signin"]["login"].value,
	    password = document.forms["signin"]["password"].value;  
	
	
// login
	if (!login) {
	    errLogin.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	if (login && login.search(/[a-z]/i) !== 0) {
		errLogin.innerHTML = WRONG_FIRST_SYMBOL;
		alertMessage += "${login}:       ${firstsymbol}; \n";
		result = false;
	} 
	var loginRegex = /\w{5,}/;
	if (login && login.search(/[a-z]/i) === 0 && 
		!loginRegex.test(login)) {
		errLogin.innerHTML = WRONG_SYMBOL_QUANT;
		alertMessage += "${login}:      ${loginsymbols}; \n";
		result = false;
	}  
		
// password
	if (!password) {
		errPassword.innerHTML = EMPTY_FIELD; 
		result = false;
	}
	var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@$!%*?&]{6,}$/;
	if (password && !passwordRegex.test(password)) {
		errPassword.innerHTML = WRONG_PSWD;
		alertMessage += "${password}:     min: ${passsymbols}; \n";
		result = false; 
	}

	
	if (alertMessage){
		alert(alertMessage);
	}
		
return result;
}



