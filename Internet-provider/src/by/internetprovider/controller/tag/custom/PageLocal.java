package by.internetprovider.controller.tag.custom;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Class is a handler for a custom tag &lt;pagelocale /&gt;.
 * 
 * @author Pavel Pranovich
 *
 */
public class PageLocal extends TagSupport {

	private static final long serialVersionUID = 1L;

	private String page;

	public void setPage(String page) {
		this.page = page;
	}

	@Override
	public int doStartTag() {

		HttpSession session = pageContext.getSession();
		String local = (String) session.getAttribute("local");
		if (local == null) {
			session.setAttribute("local", "en");
		}
		session.setAttribute("page", page);

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() {
		return EVAL_PAGE;
	}

}
