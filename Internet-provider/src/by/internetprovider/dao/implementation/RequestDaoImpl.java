package by.internetprovider.dao.implementation;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.dao.RequestDao;
import by.internetprovider.dao.exception.DaoException;

/**
 * Class that implements operations with client requests on the DAL (Data Access
 * Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public class RequestDaoImpl extends CommonDaoImpl implements RequestDao {

	@Override
	public void registerClientRequest(String login, byte accountNumber, String tariffName) throws DaoException {

		performUpdateRequest("INSERT INTO `users_requests` (`login`, `account_number`, `t_p_name`) VALUES ('" + login
				+ "', " + accountNumber + ", '" + tariffName + "');");

	}

	@Override
	public Set<ClientRequest> getRequests() throws DaoException {

		List<Map<String, String>> resultList = performSelectRequest(
				"SELECT login, account_number, t_p_name FROM users_requests");

		Set<ClientRequest> userRequests = handleResultList(resultList);

		return userRequests;
	}

	public Set<ClientRequest> getRequests(String userLogin) throws DaoException {

		List<Map<String, String>> resultList = performSelectRequest(
				"SELECT login, account_number, t_p_name FROM users_requests WHERE login = '" + userLogin + "'");

		Set<ClientRequest> userRequests = handleResultList(resultList);

		return userRequests;
	}

	@Override
	public void performAcceptedClientRequest(ClientRequest userRequest) throws DaoException {

		String login = userRequest.getUserLogin();
		String tariffName = userRequest.getTariffName();
		byte accountNum = userRequest.getAccountNumber();

		performUpdateRequest("UPDATE users_accounts SET" + "`t_p_name`='" + tariffName + "', "
				+ "`acc_balance`=`acc_balance` - (SELECT user_charge FROM tariff_plans WHERE t_p_name='" + tariffName
				+ "'), " + "`unused_traffic` = IF((SELECT traffic_volume FROM tariff_plans WHERE t_p_name='"
				+ tariffName + "') > 0, (SELECT traffic_volume FROM tariff_plans WHERE t_p_name='" + tariffName
				+ "'), -1)," + "`traffic_start_date`= CURDATE(), "
				+ "`traffic_end_date`= DATE_ADD(`traffic_start_date` , "
				+ "      INTERVAL (SELECT billing_period FROM tariff_plans WHERE t_p_name='" + tariffName + "') DAY)"
				+ "WHERE `id`=(SELECT id FROM users WHERE login='" + login + "') and`account_number`='" + accountNum
				+ "';");

		deleteRequest(userRequest);
	}

	@Override
	public void deleteRequest(ClientRequest clientRequest) throws DaoException {

		String login = clientRequest.getUserLogin();
		String tariffName = clientRequest.getTariffName();
		byte accountNum = clientRequest.getAccountNumber();

		performUpdateRequest("DELETE FROM users_requests WHERE login = '" + login + "' " + "AND account_number = "
				+ accountNum + " AND t_p_name = '" + tariffName + "'");

	}

	@Override
	public void cancelClientRequest(ClientRequest clientRequest) throws DaoException {

		deleteRequest(clientRequest);

	}

	/*
	 * Auxiliary method for converting a collection of the
	 * List<Map<String, String>> type for data retrieved from the database to a
	 * collection of the type Set<ClientRequest>.
	 */
	private Set<ClientRequest> handleResultList(List<Map<String, String>> list) {

		Set<ClientRequest> userRequests = new TreeSet<ClientRequest>();

		for (Map<String, String> map : list) {
			String login = map.get("login");

			byte accountNumber = Byte.parseByte(map.get("account_number"));
			String tariffName = map.get("t_p_name");

			ClientRequest account = new ClientRequest(login, accountNumber, tariffName);

			userRequests.add(account);
		}

		return userRequests;
	}
}
