package by.internetprovider.service.factory;

import by.internetprovider.service.AccountService;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.TariffService;
import by.internetprovider.service.UserService;
import by.internetprovider.service.implementation.AccountServiceImpl;
import by.internetprovider.service.implementation.RequestServiceImpl;
import by.internetprovider.service.implementation.TariffServiceImpl;
import by.internetprovider.service.implementation.UserServiceImpl;

/**
 * Factory class for getting references to objects whose classes implement
 * interfaces on Service Layer.
 * 
 * 
 * @author Pavel Pranovich
 *
 */
public class ServiceFactory {

	private static final ServiceFactory instance = new ServiceFactory();

	private final UserService userServiceImpl = new UserServiceImpl();

	private final AccountService accountServiceImpl = new AccountServiceImpl();

	private final RequestService requestServiceImpl = new RequestServiceImpl();

	private final TariffService tariffServiceImpl = new TariffServiceImpl();

	private ServiceFactory() {
	}

	public static ServiceFactory getInstance() {
		return instance;
	}

	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}

	public AccountService getAccountServiceImpl() {
		return accountServiceImpl;
	}

	public RequestService getRequestServiceImpl() {
		return requestServiceImpl;
	}

	public TariffService getTariffServiceImpl() {
		return tariffServiceImpl;
	}

}
