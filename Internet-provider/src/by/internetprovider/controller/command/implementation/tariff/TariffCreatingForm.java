package by.internetprovider.controller.command.implementation.tariff;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;

/**
 * Class is used to represent the administrator with a form for creating a new
 * tariff plan.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffCreatingForm implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession(true);
		session.setAttribute("userAction", "showTariffCreatingForm");

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
