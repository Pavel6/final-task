package by.internetprovider.dao.implementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.naming.NamingException;

import by.internetprovider.dao.exception.DaoException;
import by.internetprovider.dao.factory.DaoFactory;

/**
 * Class that implements the operations of executing SQL-requests to a database
 * and retrieving the results of SQL-requests.
 * 
 * @author Pavel Pranovich
 *
 */
public class CommonDaoImpl {

	/**
	 * Performs a given SQL-request of the SELECT type to the database.
	 * 
	 * @param sqlRequest
	 *            - SQL-request.
	 * @return list of SQL-request results.
	 * @throws DaoException
	 */
	public List<Map<String, String>> performSelectRequest(String sqlRequest) throws DaoException {

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		List<Map<String, String>> resultList = null;

		try {
			Class.forName("org.gjt.mm.mysql.Driver");

			DaoFactory daoFactory = DaoFactory.getInstance();
			con = daoFactory.getConnection();

			st = con.createStatement();

			rs = st.executeQuery(sqlRequest);

			resultList = convertResultSetToArrayList(rs);

		} catch (ClassNotFoundException e) {
			throw new DaoException(e);
		} catch (SQLException e) {
			throw new DaoException(e);
		} catch (NamingException e) {
			throw new DaoException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DaoException(e);
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					throw new DaoException(e);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					throw new DaoException(e);
				}
			}
		}

		return resultList;
	}

	/**
	 * Performs a specified SQL-request of the UPDATE type to the database.
	 * 
	 * @param sqlRequest
	 *            - SQL-request.
	 * @throws DaoException
	 */
	public void performUpdateRequest(String sqlRequest) throws DaoException {

		Connection con = null;
		Statement st = null;

		try {
			Class.forName("org.gjt.mm.mysql.Driver");

			DaoFactory daoFactory = DaoFactory.getInstance();
			con = daoFactory.getConnection();

			st = con.createStatement();

			st.executeUpdate(sqlRequest);

		} catch (ClassNotFoundException e) {
			throw new DaoException(e);
		} catch (SQLException e) {
			throw new DaoException(e);
		} catch (NamingException e) {
			throw new DaoException(e);
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					throw new DaoException(e);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					throw new DaoException(e);
				}
			}
		}

	}

	/**
	 * Converts the request result <code>ResultSet</code> into a collection of
	 * type <code>List&lt;Map&lt;String, String&gt;&gt;</code>, where List is a
	 * collection of records from the database, where each record (row) is
	 * represented as a <code>HashMap</code>, where each hashmap is a mapping
	 * from column name and the record data.
	 * 
	 * @param rs
	 *            - database result set
	 * @return <code>List&lt;Map&lt;String, String&gt;&gt;</code> - list of
	 *         request results.
	 * @throws DaoException
	 */
	private List<Map<String, String>> convertResultSetToArrayList(ResultSet rs) throws DaoException {

		List<Map<String, String>> resultList = null;

		try {
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			resultList = new CopyOnWriteArrayList<Map<String, String>>();
			while (rs.next()) {
				Map<String, String> row = new HashMap<String, String>(columns);
				for (int i = 1; i <= columns; ++i) {
					row.put(md.getColumnName(i), rs.getString(i));
				}
				resultList.add(row);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		}

		return resultList;
	}
}
