<%@ page contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.internetprovider.localization.local"
	var="loc" />


<fmt:message key="local.emptyfield" bundle="${loc}" var="emptyfield" />
<fmt:message key="local.passnotmatch" bundle="${loc}" var="passnotmatch" />
<fmt:message key="local.incorrectname" bundle="${loc}" var="incorrectname" />
<fmt:message key="local.incorrectnumber" bundle="${loc}" var="incorrectnumber" />
<fmt:message key="local.startwordcyrillic" bundle="${loc}" var="startwordcyrillic" />
<fmt:message key="local.realnumber" bundle="${loc}" var="realnumber" />
<fmt:message key="local.transferrate2" bundle="${loc}" var="transferrate2" />
<fmt:message key="local.trafficvolume2" bundle="${loc}" var="trafficvolume2" />
<fmt:message key="local.billingperiod2" bundle="${loc}" var="billingperiod2" />
<fmt:message key="local.integer13" bundle="${loc}" var="integer13" />
<fmt:message key="local.integer12" bundle="${loc}" var="integer12" />
<fmt:message key="local.name" bundle="${loc}" var="name" />
<fmt:message key="local.charge2" bundle="${loc}" var="charge2" />


function validateCreatingTariffForm() {
	var result = true; 
	
	// for alert about wrong fields
	var alertMessage = "";
	
	var EMPTY_FIELD = " ${emptyfield}",
		WRONG_NAME = " ${incorrectname}",
		WRONG_NUMBER = " ${incorrectnumber}";
	
	var errName = document.getElementById("errName"),
	    errCharge = document.getElementById("errCharge"),
		errRate = document.getElementById("errRate"),
		errVolume = document.getElementById("errVolume"),
	 	errBilling = document.getElementById("errBilling");
		
	errName.innerHTML = "";
	errCharge.innerHTML = "";
	errRate.innerHTML = "";
	errVolume.innerHTML = "";
	errBilling.innerHTML = "";
		
	var tariffName = document.forms["newTar"]["tariffname"].value,    
		charge = document.forms["newTar"]["charge"].value,     
		internetRate = document.forms["newTar"]["internetrate"].value,  
        trafficVolume = document.forms["newTar"]["trafficvolume"].value,
        billingPeriod = document.forms["newTar"]["billingperiod"].value;
	
// tariffName
	if (!tariffName) {
	    errName.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var tariffNameRegex = /^[\u0410-\u042F]{1}[\u0430-\u044F\u0410-\u042F ]{1,10}$/;
	if (tariffName && !tariffNameRegex.test(tariffName)) {
		errName.innerHTML = WRONG_NAME;
		alertMessage += "${name}:    ${startwordcyrillic}; \n";
		result = false;
	} 
	
// charge
	if (!charge) {
		errCharge.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var chargeRegex = /^(\d+\.)?\d+$/;
	if (charge && !chargeRegex.test(charge)) {
		errCharge.innerHTML = WRONG_NUMBER;
		alertMessage += "${charge2}:    ${realnumber}; \n";
		result = false;
	} 
	
// internetRate
	if (!internetRate) {
		errRate.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var internetRateRegex = /\d{1,3}/;
	if (internetRate && !internetRateRegex.test(internetRate)) {
		errRate.innerHTML = WRONG_NUMBER;
		alertMessage += "${transferrate2}:    ${integer13}; \n";
		result = false;
	}
	
// trafficVolume
	if (!trafficVolume) {
		errVolume.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var trafficVolumeRegex = /\d{1,2}/;
	if (trafficVolume && !trafficVolumeRegex.test(trafficVolume)) {
		errVolume.innerHTML = WRONG_NUMBER;
		alertMessage += "${trafficvolume2}:    ${integer12}; \n";
		result = false;
	}
	
// billingPeriod
	if (!billingPeriod) {
		errBilling.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var billingPeriodRegex = /\d{1,3}/;
	if (billingPeriod && !billingPeriodRegex.test(billingPeriod)) {
		errBilling.innerHTML = WRONG_NUMBER;
		alertMessage += "${billingperiod2}:    ${integer13}; \n";
		result = false;
	}
	
	if (alertMessage){
		alert(alertMessage);
	}
	
return result;
}


