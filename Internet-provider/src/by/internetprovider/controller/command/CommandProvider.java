package by.internetprovider.controller.command;

import java.util.HashMap;
import java.util.Map;

import by.internetprovider.controller.command.implementation.Localization;
import by.internetprovider.controller.command.implementation.PageTransition;
import by.internetprovider.controller.command.implementation.SignIn;
import by.internetprovider.controller.command.implementation.SignOut;
import by.internetprovider.controller.command.implementation.account.AccountChoice;
import by.internetprovider.controller.command.implementation.account.AccountCreating;
import by.internetprovider.controller.command.implementation.account.AccountReview;
import by.internetprovider.controller.command.implementation.account.BalanceRaising;
import by.internetprovider.controller.command.implementation.account.BanChanging;
import by.internetprovider.controller.command.implementation.account.ClientAccountOverview;
import by.internetprovider.controller.command.implementation.account.FillingBalanceForm;
import by.internetprovider.controller.command.implementation.request.ClientRequestsView;
import by.internetprovider.controller.command.implementation.request.ClientTariffChanging;
import by.internetprovider.controller.command.implementation.request.RequestCancellation;
import by.internetprovider.controller.command.implementation.request.ClientRequestsAdminView;
import by.internetprovider.controller.command.implementation.request.TariffChoiceRegistration;
import by.internetprovider.controller.command.implementation.tariff.TariffViewAndChoice;
import by.internetprovider.controller.command.implementation.tariff.TariffCreatingForm;
import by.internetprovider.controller.command.implementation.tariff.TariffFormation;
import by.internetprovider.controller.command.implementation.tariff.TariffReview;
import by.internetprovider.controller.command.implementation.user.ClientRegistration;
import by.internetprovider.controller.command.implementation.user.UserReview;

/**
 * A class intended to obtain a reference to an object that implements the
 * <code>Command</code> interface, given a specified input command.
 * 
 * @author Pavel Pranovich
 *
 */
public class CommandProvider {
	/**
	 * Map for command names as keys and objects that implement the
	 * <code>Command</code> interface as values.
	 */
	private Map<CommandName, Command> commands = new HashMap<>();

	/**
	 * Performs a record of the objects "key" - "value".
	 */
	public CommandProvider() {
		commands.put(CommandName.REGISTRATE, new ClientRegistration());
		commands.put(CommandName.LOCALISE, new Localization());
		commands.put(CommandName.GOTO_PAGE, new PageTransition());
		commands.put(CommandName.SIGNIN, new SignIn());
		commands.put(CommandName.SIGNOUT, new SignOut());
		commands.put(CommandName.REVIEW_TARIFFS, new TariffReview());
		commands.put(CommandName.REVIEW_USERS, new UserReview());
		commands.put(CommandName.REVIEW_ACCOUNTS, new AccountReview());
		commands.put(CommandName.REVIEW_ACCOUNT, new ClientAccountOverview());
		commands.put(CommandName.CREATE_ACCOUNT, new AccountCreating());
		commands.put(CommandName.CHANGE_BAN, new BanChanging());
		commands.put(CommandName.FORM_TARIFF, new TariffFormation());
		commands.put(CommandName.SHOW_TARIFF_FORM, new TariffCreatingForm());
		commands.put(CommandName.REFILL_ACCOUNT, new FillingBalanceForm());
		commands.put(CommandName.ENHANCE_BALANCE, new BalanceRaising());
		commands.put(CommandName.SELECT_CHANGE_TARIFF, new AccountChoice());
		commands.put(CommandName.SHOW_CHOICE_FORM, new TariffViewAndChoice());
		commands.put(CommandName.REGISTER_TARIFF_CHOICE, new TariffChoiceRegistration());
		commands.put(CommandName.SHOW_TARIFFCHANGE_FORM, new ClientRequestsAdminView());
		commands.put(CommandName.CHANGE_USERTARIFF, new ClientTariffChanging());
		commands.put(CommandName.SHOW_SENT_REQUESTS, new ClientRequestsView());
		commands.put(CommandName.CANCEL_REQUEST, new RequestCancellation());
	}

	/**
	 * Returns a reference to an object that implements the <code>Command</code>
	 * interface for the given command name.
	 * 
	 * @param name
	 *            - command name.
	 * @return object that implements the <code>Command</code> interface.
	 */
	public Command getCommand(String name) {
		CommandName commandName;

		commandName = CommandName.valueOf(name.toUpperCase());

		return commands.get(commandName);
	}

}
