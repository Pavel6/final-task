package by.internetprovider.controller.command.implementation.request;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.controller.command.Command;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to implement the cancellation of the request by the client.
 * Client can cancel only own requests.
 * 
 * @author Pavel Pranovich
 *
 */
public class RequestCancellation implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		String login = request.getParameter("login");
		byte clientAccountNumber = Byte.parseByte(request.getParameter("accountnumber"));
		String newTariffName = request.getParameter("newtariffname");

		ClientRequest clientRequest = new ClientRequest(login, clientAccountNumber, newTariffName);

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		RequestService requestServiceImpl = serviceFactory.getRequestServiceImpl();
		requestServiceImpl.cancelRequest(clientRequest);

		ClientRequestsView requestsForm = new ClientRequestsView();
		requestsForm.execute(request, response);

	}

}
