package by.internetprovider.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Class is intended for modifying the encoding of the request.
 * 
 * @author Pavel Pranovich
 *
 */
public class CharsetFilter implements Filter {

	private String encoding;

	// for testing only
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

		encoding = config.getInitParameter("requestEncoding");
		if (encoding == null) {
			encoding = "UTF-8";
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (request.getCharacterEncoding() == null) {
			request.setCharacterEncoding(encoding);
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

		encoding = null;
	}
}
