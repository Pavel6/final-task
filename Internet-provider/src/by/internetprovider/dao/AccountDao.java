package by.internetprovider.dao;

import java.util.Set;

import by.internetprovider.bean.Account;
import by.internetprovider.dao.exception.DaoException;

/**
 * This interface defines operations with elements of the accounts of clients on
 * the DAL (Data Access Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */

public interface AccountDao {

	/**
	 * Returns the accounts of all clients.
	 * 
	 * @return accounts of clients.
	 * @throws DaoException
	 */
	public Set<Account> getClientAccounts() throws DaoException;

	/**
	 * Returns the accounts of a client with specified login.
	 * 
	 * @param login
	 *            - client login.
	 * @return accounts of client.
	 * @throws DaoException
	 */
	public Set<Account> getClientAccounts(String login) throws DaoException;

	/**
	 * Creates a new account for a client with the specified login.
	 * 
	 * @param login
	 *            - client login.
	 * @throws DaoException
	 */
	public void createClientAccount(String login) throws DaoException;

	/**
	 * Refills the balance (the amount of payment means) by a certain amount on
	 * the specified account of the client with the given login.
	 * 
	 * @param login
	 *            - client login.
	 * @param accountNumber
	 *            - client account number.
	 * @param refill
	 *            - Amount of replenished means of payment.
	 * @throws DaoException
	 */
	public void raiseClientBalance(String login, byte accountNumber, float refill) throws DaoException;

	/**
	 * Changes the status of ban on a particular client's account with the
	 * specified login.
	 * 
	 * @param login
	 *            - client login.
	 * @param accountNumber
	 *            - client account number.
	 * @param currentBan
	 *            - current ban status.
	 * @throws DaoException
	 */
	public void changeBan(String login, byte accountNumber, boolean currentBan) throws DaoException;
}
