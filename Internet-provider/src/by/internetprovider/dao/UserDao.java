package by.internetprovider.dao;

import java.util.Set;

import by.internetprovider.bean.RegUser;
import by.internetprovider.bean.User;
import by.internetprovider.dao.exception.DaoException;

/**
 * This interface defines operations with users (administrators and clients) on
 * the DAL (Data Access Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public interface UserDao {

	/**
	 * Registers the specified new client.
	 * 
	 * @param newClient
	 *            - new client
	 * @return <code>true</code> if login doesn't exist in database,
	 *         <code>false</code> if login exists in database.
	 * @throws DaoException
	 */
	public boolean registrateClient(RegUser newClient) throws DaoException;

	/**
	 * Performs user authorization.
	 * 
	 * @param login
	 *            - user login.
	 * @param password
	 *            - user password.
	 * @return User corresponding to the given login and password.
	 * @throws DaoException
	 */
	public User signIn(String login, String password) throws DaoException;

	/**
	 * Returns the set of all users.
	 * 
	 * @return set of users.
	 * @throws DaoException
	 */
	public Set<User> getUsers() throws DaoException;

}
