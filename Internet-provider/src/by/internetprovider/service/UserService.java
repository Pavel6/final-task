package by.internetprovider.service;

import java.util.Set;

import by.internetprovider.bean.RegFormCheckout;
import by.internetprovider.bean.RegUser;
import by.internetprovider.bean.User;
import by.internetprovider.service.exception.ServiceException;

/**
 * This interface defines operations with users (administrators and clients) on
 * the Service Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public interface UserService {

	/**
	 * Registers the specified new client.
	 * 
	 * @param newClient
	 *            - new client.
	 * @return <code>{@link RegFormCheckout}</code> instance for results of
	 *         validation for the registration form of the new client.
	 * @throws ServiceException
	 */
	public RegFormCheckout registrateClient(RegUser newClient) throws ServiceException;

	/**
	 * Performs user authorization.
	 * 
	 * @param login
	 *            - user login.
	 * @param password
	 *            - user password.
	 * @return User corresponding to the given login and password.
	 * @throws ServiceException
	 */
	public User signIn(String login, String password) throws ServiceException;

	/**
	 * Returns the set of all users.
	 * 
	 * @return set of users.
	 * @throws ServiceException
	 */
	public Set<User> getUsers() throws ServiceException;
}
