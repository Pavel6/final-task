<%@ page contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.internetprovider.localization.local"
	var="loc" />
	
<fmt:message key="local.addmail" bundle="${loc}" var="addmail" />
<fmt:message key="local.delete" bundle="${loc}" var="delete" />
<fmt:message key="local.emptyfield" bundle="${loc}" var="emptyfield" />
<fmt:message key="local.passnotmatch" bundle="${loc}" var="passnotmatch" />
<fmt:message key="local.incorrectusername" bundle="${loc}" var="incorrectusername" />
<fmt:message key="local.wrongfirstsymbol" bundle="${loc}" var="wrongfirstsymbol" />
<fmt:message key="local.notenoughsymbols" bundle="${loc}" var="notenoughsymbols" />
<fmt:message key="local.incorrectpass" bundle="${loc}" var="incorrectpass" />
<fmt:message key="local.incorrectnumber" bundle="${loc}" var="incorrectnumber" />
<fmt:message key="local.incorrectage" bundle="${loc}" var="incorrectage" />

<fmt:message key="local.startwordlatin" bundle="${loc}" var="startwordlatin" />
<fmt:message key="local.firstsymbol" bundle="${loc}" var="firstsymbol" />
<fmt:message key="local.loginsymbols" bundle="${loc}" var="loginsymbols" />
<fmt:message key="local.passsymbols" bundle="${loc}" var="passsymbols" />
<fmt:message key="local.agerange" bundle="${loc}" var="agerange" />
<fmt:message key="local.years" bundle="${loc}" var="years" />
<fmt:message key="local.mailsymbols" bundle="${loc}" var="mailsymbols" />
<fmt:message key="local.alt" bundle="${loc}" var="alt" />
<fmt:message key="local.incorrectmail" bundle="${loc}" var="incorrectmail" />
<fmt:message key="local.startwordlatin" bundle="${loc}" var="startwordlatin" />
<fmt:message key="local.username" bundle="${loc}" var="username" />
<fmt:message key="local.usersurname" bundle="${loc}" var="usersurname" />
<fmt:message key="local.login" bundle="${loc}" var="login" />
<fmt:message key="local.password" bundle="${loc}" var="password" />
<fmt:message key="local.age" bundle="${loc}" var="age" />


function validateRegForm() {
	var result = true; 
	
	// for alert about wrong fields
	var alertMessage = "";
	
	var EMPTY_FIELD = " ${emptyfield}",
		PSWD_NOT_MATCH = " ${passnotmatch}",
		WRONG_NAME = " ${incorrectusername}",
		WRONG_FIRST_SYMBOL = " ${wrongfirstsymbol}",
		WRONG_SYMBOL_QUANT = " ${notenoughsymbols}",
		WRONG_PSWD = " ${incorrectpass}",
		WRONG_AGE = " ${incorrectage}",
		WRONG_NUMB = " ${incorrectnumber}",
		WRONG_MAIL = " ${incorrectmail}";
	
	var errName = document.getElementById("errName"),
		errSurname = document.getElementById("errSurname"),
		errLogin = document.getElementById("errLogin"),
		errPassword1 = document.getElementById("errPassword1"),
		errPassword2= document.getElementById("errPassword2"),
		errAge = document.getElementById("errAge"),
	 	errMail = document.getElementById("errMail"),
		errMailAlt = document.getElementById("errMailAlt");
		
	errName.innerHTML = "";
	errSurname.innerHTML = "";
	errLogin.innerHTML = "";
	errPassword1.innerHTML = "";
	errPassword2.innerHTML = "";
	errAge.innerHTML = "";
	errMail.innerHTML = "";
		
	var name = document.forms["registration"]["name"].value,    
		surname = document.forms["registration"]["surname"].value,     
		login = document.forms["registration"]["login"].value,  
        password1 = document.forms["registration"]["password1"].value,
        password2 = document.forms["registration"]["password2"].value,
        age = document.forms["registration"]["age"].value,
        mail = document.forms["registration"]["mail"].value,
        mailAlt = null;
		
	if (document.forms["registration"]["mailAlt"] !== undefined)	{
		mailAlt = document.forms["registration"]["mailAlt"].value;	
	}
	
// userName
	if (!name) {
	    errName.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var nameRegex = /^[A-Z]{1}[a-zA-Z ]{1,10}$/;
	if (name && !nameRegex.test(name)) {
		errName.innerHTML = WRONG_NAME;
		alertMessage += "${username}:          ${startwordlatin}; \n";
		result = false;
	} 
// userSurname
	if (!surname) {
	    errSurname.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	var surnameRegex = /^[A-Z]{1}[a-zA-Z ]{1,15}$/;
	if (surname && !surnameRegex.test(surname)) {
		errSurname.innerHTML = WRONG_NAME;
		alertMessage += "${usersurname}:  ${startwordlatin}; \n";
		result = false;
	} 
	
// userLogin
	if (!login) {
	    errLogin.innerHTML = EMPTY_FIELD;
		result = false;
	} 
	if (login && login.search(/[a-z]/i) !== 0) {
		errLogin.innerHTML = WRONG_FIRST_SYMBOL;
		alertMessage += "${login}:       ${firstsymbol}; \n";
		result = false;
	} 
	var loginRegex = /\w{5,}/;
	if (login && login.search(/[a-z]/i) === 0 && 
		!loginRegex.test(login)) {
		errLogin.innerHTML = WRONG_SYMBOL_QUANT;	
		alertMessage += "${login}:       ${loginsymbols}; \n";
		result = false;
	} 
// password
	if (!password1) {
		errPassword1.innerHTML = EMPTY_FIELD; 
		result = false;
	}
	var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@$!%*?&]{6,}$/;
	if (password1 && !passwordRegex.test(password1)) {
		errPassword1.innerHTML = WRONG_PSWD;
		alertMessage += "${password}:     min: ${passsymbols}; \n";
		result = false; 
	}
	if (!password2) {
		errPassword2.innerHTML = EMPTY_FIELD; 
		result = false;
	}  
	if (password1 && password2 && password1 !== password2) {
		errPassword2.innerHTML = PSWD_NOT_MATCH;
        password1 = "";   
        password2 = "";
		result = false;
	}
// age
	if (!age) {
		errAge.innerHTML = EMPTY_FIELD; 
		result = false;
	}
	var ageRegex = /\s[0-1]{1}[0-9]{0,2}/; 
	if (age && !ageRegex.test(age)) {
		errAge.innerHTML = WRONG_NUMB; 
		alertMessage += "${age}:    ${agerange}; \n";
		result = false;
	}
	if (age && age < 7) {
		errAge.innerHTML = WRONG_AGE; 
		alertMessage += "${age}:    min: 7 ${years}; \n";
		result = false;
	}
	if (age && age > 120) {
		errAge.innerHTML = WRONG_AGE; 
		alertMessage += "${age}:    max: 120 ${years}; \n";
		result = false;
	}
// e-mail
	if (!mail) {
		errMail.innerHTML = EMPTY_FIELD; 
		result = false;
	} 	
	var mailRegex = /\S+@\S+\.\S+/; 
	if (mail && !mailRegex.test(mail)) {
		errMail.innerHTML = WRONG_MAIL;
		alertMessage += "E-mail:       ${mailsymbols}; \n";
		result = false;
	}
// alternative e-mail 
	if (mailAlt !== null && !mailAlt) {
		errMailAlt.innerHTML = EMPTY_FIELD; 
		result = false;
	} 	

	if (mailAlt !== null && mailAlt && !mailRegex.test(mailAlt)) {
		errMailAlt.innerHTML = WRONG_MAIL;
	    alertMessage += "E-mail:       ${mailsymbols}; \n";
		result = false;
	}
	
	if (alertMessage){
		alert(alertMessage);
	}
	
return result;
}

function addAltMail() {
	var regInputs = document.getElementById("regInputs");
	
	var raw = document.createElement("TR"),
	    nameTd = document.createElement("TD"),
		inputTd = document.createElement("TD"),
		spanTd = document.createElement("TD"),
		buttonTd = document.createElement("TD"),
	    rawId = document.createAttribute("ID"); 
		
	rawId.value = "altMail";
	raw.setAttributeNode(rawId);
	
	nameTd.innerHTML = "E-mail (${alt}):";
	inputTd.innerHTML = '<input type="text" name="mailAlt" value="" title="E-mail">';
	spanTd.innerHTML = '<span id="errMailAlt" class="errMessage"> </span>';	
	buttonTd.innerHTML = '<button id="deleteButton" onclick="deleteAltMail()">${delete}</button>';	
	
	raw.appendChild(nameTd);
	raw.appendChild(inputTd);
	raw.appendChild(spanTd);
	raw.appendChild(buttonTd);
	
	regInputs.appendChild(raw);
	
	var addMailButton = document.getElementById("addMailButton");
	addMailButton.remove();
}


function deleteAltMail(){
	var altMail = document.getElementById("altMail");
	altMail.remove();

	var mailRaw = document.getElementById("mailRaw"),
	    returnAddButton = document.createElement("TD"),
	    rawButtonId = document.createAttribute("ID"); 
		 				
	rawButtonId.value = "addMailButton";
    returnAddButton.setAttributeNode(rawButtonId);	
	
	returnAddButton.innerHTML = '<button type="button" id="addMail" onclick="addAltMail()">${addmail}</button>';
	
	mailRaw.appendChild(returnAddButton);
}



