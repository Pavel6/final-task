package by.internetprovider.controller.tag.custom;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.internetprovider.controller.InternetProviderController;

/**
 * Class is a handler for a custom tag &lt;signinerror /&gt;.
 * 
 * @author Pavel Pranovich
 *
 */
public class SignInError extends TagSupport {

	private static Logger logger = Logger.getLogger(InternetProviderController.class.getName());

	private static final long serialVersionUID = 1L;

	@Override
	public int doStartTag() {

		HttpSession session = pageContext.getSession();
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

		String signInError = (String) session.getAttribute("signinError");

		try {
			JspWriter out = pageContext.getOut();

			if (signInError != null) {
				out.write(signInError);
			}

		} catch (IOException e) {
			logger.info(e.getCause());

			RequestDispatcher dispatch = request.getRequestDispatcher("/page/error.jsp");
			try {
				dispatch.forward(request, response);
			} catch (ServletException | IOException e1) {
				logger.info(e1.getCause());
			}
		}

		session.removeAttribute("signinError");

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() {
		return EVAL_PAGE;
	}

}
