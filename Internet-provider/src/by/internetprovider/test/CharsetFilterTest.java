package by.internetprovider.test;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import by.internetprovider.controller.filter.CharsetFilter;

public class CharsetFilterTest extends Mockito {

	private static Logger testLog = Logger.getLogger(CharsetFilterTest.class.getName());

	static {
		PropertyConfigurator.configure("src/by/internetprovider/test/log/config/log4j.properties");
	}

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Before
	public void init() {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}

	@Test
	public void testCharsetEncoding() {

		request.getSession();

		CharsetFilter filter = new CharsetFilter();
		filter.setEncoding("UTF-8");

		FilterChain filterChain = mock(FilterChain.class);

		try {
			filter.doFilter(request, response, filterChain);
		} catch (IOException | ServletException e) {
			testLog.info(e.getMessage());
		}

		Assert.assertEquals("UTF-8", request.getCharacterEncoding());
	}

}
