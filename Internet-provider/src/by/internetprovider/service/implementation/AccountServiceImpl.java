package by.internetprovider.service.implementation;

import java.util.Set;

import by.internetprovider.bean.Account;
import by.internetprovider.dao.AccountDao;
import by.internetprovider.dao.exception.DaoException;
import by.internetprovider.dao.factory.DaoFactory;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.exception.ServiceException;

/**
 * Class that implements operations with the elements of client accounts on the
 * Service Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public class AccountServiceImpl implements AccountService {

	@Override
	public Set<Account> getClientAccounts() throws ServiceException {

		AccountDao accountDaoImpl = getAccountDaoImpl();

		Set<Account> accounts;

		try {
			accounts = accountDaoImpl.getClientAccounts();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return accounts;
	}

	@Override
	public Set<Account> getClientAccounts(String login) throws ServiceException {

		AccountDao accountDaoImpl = getAccountDaoImpl();

		Set<Account> accounts;

		try {
			accounts = accountDaoImpl.getClientAccounts(login);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return accounts;
	}

	@Override
	public Account getClientAccount(String login, byte accountNumber) throws ServiceException {

		AccountDao accountDaoImpl = getAccountDaoImpl();

		Set<Account> accounts;

		try {
			accounts = accountDaoImpl.getClientAccounts(login);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		Account approprAccount = null;
		for (Account account : accounts) {
			if (account.getAccountNumber() == accountNumber) {
				approprAccount = account;
				break;
			}
		}

		return approprAccount;
	}

	@Override
	public void createClientAccount(String login) throws ServiceException {

		AccountDao accountDaoImpl = getAccountDaoImpl();

		try {
			accountDaoImpl.createClientAccount(login);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void changeBan(String login, byte accountNumber, boolean currentBan) throws ServiceException {

		AccountDao accountDaoImpl = getAccountDaoImpl();

		try {
			accountDaoImpl.changeBan(login, accountNumber, currentBan);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public boolean raiseBalance(String login, byte accountNumber, String refill) throws ServiceException {

		boolean isValidatedRefill = validateRefill(refill);

		if (!isValidatedRefill) {
			return false;
		}

		float refillValue = Float.parseFloat(refill);

		AccountDao accountDaoImpl = getAccountDaoImpl();

		try {
			accountDaoImpl.raiseClientBalance(login, accountNumber, refillValue);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return true;
	}

	/**
	 * Carries out the validation of the input amount of replenished means of
	 * payment.
	 * 
	 * @param refill
	 *            - amount of replenished means of payment.
	 * @return <code>true</code> if the value <code>refill</code> meets the
	 *         validation requirements, <code>false</ code> - otherwise.
	 * @see Account
	 */
	private boolean validateRefill(String refill) {
		boolean isValidated = true;

		String REGEX_REFILL = "[0-9]*\\.?[0-9]{2}";

		if (refill.isEmpty()) {
			isValidated = false;
		}

		if (!refill.matches(REGEX_REFILL)) {
			isValidated = false;
		}

		return isValidated;
	}

	/*
	 * Auxiliary method for obtaining a reference to an object whose class
	 * implements the interface AccountDao.
	 */
	private AccountDao getAccountDaoImpl() {

		DaoFactory daoFactory = DaoFactory.getInstance();
		AccountDao accountDaoImpl = daoFactory.getAccountDaoImpl();

		return accountDaoImpl;
	}
}
