package by.internetprovider.dao;

import java.util.Set;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.dao.exception.DaoException;

/**
 * This interface defines operations with requests of clients on the DAL (Data
 * Access Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public interface RequestDao {

	/**
	 * Registers the client request with the specified login on the tariff plan
	 * for a certain account.
	 * <p>
	 * Registration is carried out as a record in the database.
	 * 
	 * @param clientLogin
	 *            - client login.
	 * @param accountNumber
	 *            - client account number.
	 * @param tariffName
	 *            - name of the new tariff plan chosen by the client.
	 * @throws DaoException
	 */
	public void registerClientRequest(String clientLogin, byte accountNumber, String tariffName) throws DaoException;

	/**
	 * Returns the requests of all clients for tariff plan activation.
	 * 
	 * @return client requests.
	 * @throws DaoException
	 */
	public Set<ClientRequest> getRequests() throws DaoException;

	/**
	 * Returns the requests of client with the specified login.
	 * 
	 * @param clientLogin
	 *            - client login.
	 * @return client requests.
	 * @throws DaoException
	 */
	public Set<ClientRequest> getRequests(String clientLogin) throws DaoException;

	/**
	 * Performs the specified client request (activates a new tariff plan).
	 * 
	 * @param clientRequest
	 *            - client request.
	 * @throws DaoException
	 */
	public void performAcceptedClientRequest(ClientRequest clientRequest) throws DaoException;

	/**
	 * Removes the specified client request from the database.
	 * 
	 * @param clientRequest
	 *            - client request.
	 * @throws DaoException
	 */
	public void deleteRequest(ClientRequest clientRequest) throws DaoException;

	/**
	 * Cancels the specified client request.
	 * 
	 * @param clientRequest
	 *            - client request.
	 * @throws DaoException
	 */
	public void cancelClientRequest(ClientRequest clientRequest) throws DaoException;

}
