package by.internetprovider.controller.tag.custom;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.internetprovider.controller.InternetProviderController;

/**
 * Class is a handler for a custom tag &lt;userform /&gt;.
 * 
 * @author Pavel Pranovich
 *
 */
public class UserForm extends TagSupport {

	private static Logger logger = Logger.getLogger(InternetProviderController.class.getName());

	private static final long serialVersionUID = 1L;

	private String command;
	private String submitValue;
	private String param;
	private String paramValue;

	public void setCommand(String command) {
		this.command = command;
	}

	public void setSubmitValue(String submitValue) {
		this.submitValue = submitValue;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	@Override
	public int doStartTag() {

		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

		try {
			JspWriter out = pageContext.getOut();

			out.write("<div class=\"form\">");
			out.write("<form action=\"InternetProviderController\" method=\"post\">");
			out.write("<input type=\"hidden\" name=\"" + param + "\" value=\"" + paramValue + "\" />");
			out.write("<input type=\"hidden\" name=\"command\" value=\"" + command + "\" />");
			out.write("<div class=\"submit\">");
			out.write("<input type=\"submit\" value=\"" + submitValue + "\" />");
			out.write("</div>");
			out.write("</form>");
			out.write("</div>");

		} catch (IOException e) {
			logger.info(e.getCause());

			RequestDispatcher dispatch = request.getRequestDispatcher("/page/error.jsp");
			try {
				dispatch.forward(request, response);
			} catch (ServletException | IOException e1) {
				logger.info(e1.getCause());
			}
		}

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() {
		return EVAL_PAGE;
	}

}
