package by.internetprovider.controller.command;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.internetprovider.service.exception.ServiceException;

/**
 * This interface defines operations with objects of class
 * <code>HttpServletRequest</code> and <code>HttpServletResponse</code> on the
 * Controller layer.
 * 
 * @author Pavel Pranovich
 *
 */
public interface Command {

	/**
	 * Performs operations with objects of classes
	 * <code>HttpServletRequest</code> and <code>HttpServletResponse</code>
	 * depending on the <code>command</code> parameter associated with the
	 * object <code>HttpServletRequest</code>
	 * 
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws ServletException
	 * @throws IOException
	 * @throws NamingException
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException, NamingException;

}
