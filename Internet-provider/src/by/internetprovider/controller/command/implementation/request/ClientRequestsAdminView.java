package by.internetprovider.controller.command.implementation.request;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.pagination.Pagination;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to represent an administrator with the overview of requests
 * from all clients.
 * 
 * @author Pavel Pranovich
 *
 */
public class ClientRequestsAdminView implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		HttpSession session = request.getSession(true);

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		RequestService requestServiceImpl = serviceFactory.getRequestServiceImpl();
		Set<ClientRequest> requests = requestServiceImpl.getRequests();

		Pagination.setArrayPagination(requests.toArray(), 5, request, session);

		session.setAttribute("userAction", "showRequests");
		session.setAttribute("requests", requests);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
