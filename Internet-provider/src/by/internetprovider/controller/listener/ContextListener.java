package by.internetprovider.controller.listener;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.PropertyConfigurator;

/**
 * Class for configuring the logging object.
 * 
 * @author Pavel Pranovich
 *
 */
public class ContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {

		ServletContext context = event.getServletContext();
		String log4jConfigFile = context.getInitParameter("log4j_config");
		String configFilePath = context.getRealPath("") + File.separator + log4jConfigFile;

		PropertyConfigurator.configure(configFilePath);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
	}

}
