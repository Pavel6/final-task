package by.internetprovider.controller.command.implementation;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;
import by.internetprovider.service.exception.ServiceException;

/**
 * Class for setting the locale value.
 * 
 * @author Pavel Pranovich
 *
 */
public class Localization implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		String local = request.getParameter("local");

		HttpSession session = request.getSession(true);
		session.setAttribute("local", local);

		String url = "InternetProviderController?command=goto_page";

		response.sendRedirect(url);

	}

}
