package by.internetprovider.controller.tag.custom;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocaleSupport;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.internetprovider.bean.Account;
import by.internetprovider.bean.Tariff;
import by.internetprovider.bean.User;
import by.internetprovider.bean.ClientRequest;
import by.internetprovider.controller.InternetProviderController;

/**
 * Class is a handler for a custom tag &lt;informwindow /&gt;.
 * 
 * @author Pavel Pranovich
 *
 */
public class InformWindow extends TagSupport {

	private static Logger logger = Logger.getLogger(InternetProviderController.class.getName());

	private static final long serialVersionUID = 1L;

	@Override
	public int doStartTag() throws JspException {

		HttpSession session = pageContext.getSession();
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

		String userAction = (String) session.getAttribute("userAction");

		if (userAction != null) {
			try {
				performAction(userAction, session);
			} catch (IOException e) {
				logger.info(e.getCause());

				RequestDispatcher dispatch = request.getRequestDispatcher("/page/error.jsp");
				try {
					dispatch.forward(request, response);
				} catch (ServletException | IOException e1) {
					logger.info(e1.getCause());
				}
			}
		}

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	/**
	 * Executes the user's command <code>userAction</code>.
	 * 
	 * @param userAction
	 *            - user's command.
	 * @param session
	 *            - current session.
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private void performAction(String userAction, HttpSession session) throws IOException {

		Set<Tariff> tariffs = (Set<Tariff>) session.getAttribute("tariffs");
		Set<Account> clientAccounts = (Set<Account>) session.getAttribute("clientAccounts");
		Account clientAccount = (Account) session.getAttribute("clientAccount");
		Set<ClientRequest> requests = (Set<ClientRequest>) session.getAttribute("requests");
		Set<ClientRequest> clientRequests = (Set<ClientRequest>) session.getAttribute("clientRequests");
		Set<User> users = (Set<User>) session.getAttribute("users");

		switch (userAction) {
		case "showTariffs":
			writeTariffsTable(tariffs);
			session.removeAttribute("tariffs");
			break;
		case "showUsers":
			writeUsersTable(users);
			session.removeAttribute("users");
			break;
		case "showAccounts":
			writeAccountsTable(clientAccounts);
			session.removeAttribute("accounts");
			break;
		case "showClientAccounts":
			writeClientAccountsTable(clientAccounts);
			session.removeAttribute("userAccounts");
			break;
		case "showAccountChoice":
			writeAccountChoiceForm(clientAccounts);
			session.removeAttribute("userAccounts");
			break;
		case "showTariffChoice":
			writeTariffChoiceForm(clientAccount, clientAccounts, tariffs);
			session.removeAttribute("userAccount");
			session.removeAttribute("tariffs");
			break;
		case "showRequests":
			writeRequestsForm(requests);
			session.removeAttribute("userRequests");
			break;
		case "showClientRequests":
			writeClientRequestsForm(clientRequests);
			session.removeAttribute("userRequests");
			break;
		case "showPaymentForm":
			writePaymentForm(clientAccounts);
			session.removeAttribute("userAccount");
			break;
		case "showTariffCreatingForm":
			writeTariffCreatingForm();
			break;
		}

		session.removeAttribute("userAction");
	}

	private void writeTariffsTable(Set<Tariff> tariffs) throws IOException {

		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();

		Tariff[] tariffArray = tariffs.toArray(new Tariff[tariffs.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffplans") + "</caption>");
		out.write("<thead>");
		out.write("<tr>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.name") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.trafficvolume") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.transferrate") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.billingperiod") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.charge") + "</th>");
		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");
			out.write("<td>" + tariffArray[i].getName() + "</td>");

			byte trafficVolume = tariffArray[i].getTrafficVolume();
			if (trafficVolume > 0) {
				out.write("<td>" + trafficVolume + "</td>");
			} else if (trafficVolume < 0) {
				out.write("<td>∞</td>");
			}

			out.write("<td>" + tariffArray[i].getInternetRate() + "</td>");
			out.write("<td>" + tariffArray[i].getBillingPeriod() + "</td>");
			out.write("<td>" + tariffArray[i].getCharge() + "</td>");
			out.write("</tr>");
		}
		out.write("</tbody>");
		out.write("</table>");

		writePagination("review_tariffs");
	}

	private void writeUsersTable(Set<User> users) throws IOException {

		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();

		User[] userArray = users.toArray(new User[users.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.users") + "</caption>");
		out.write("<thead>");
		out.write("<tr>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.usersurname") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.username") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.login") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.role") + "</th>");
		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");
			out.write("<td>" + userArray[i].getSurname() + "</td>");
			out.write("<td>" + userArray[i].getName() + "</td>");
			out.write("<td>" + userArray[i].getLogin() + "</td>");
			out.write("<td>" + userArray[i].getRoleName() + "</td>");
			out.write("</tr>");
		}
		out.write("</tbody>");
		out.write("</table>");

		writePagination("review_users");
	}

	private void writeAccountsTable(Set<Account> accounts) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		JspWriter out = pageContext.getOut();
		HttpSession session = pageContext.getSession();

		Account[] accountArray = accounts.toArray(new Account[accounts.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable accounttable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.clientaccounts") + "</caption>");
		out.write("<thead>");
		out.write("<tr>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.usersurname") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.username") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.balance") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffplan") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffsubscribedate") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffenddate") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.ban") + "</th>");
		out.write("<th></th>");
		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");
			out.write("<td>" + accountArray[i].getUser().getSurname() + "</td>");
			out.write("<td>" + accountArray[i].getUser().getName() + "</td>");
			out.write("<td>" + accountArray[i].getBalance() + "</td>");
			out.write("<td>" + accountArray[i].getTariffName() + "</td>");

			String startDate = null;
			if (accountArray[i].getTariffStartDate() != null) {
				startDate = formatter.format(accountArray[i].getTariffStartDate());
			}

			String endDate = null;
			if (accountArray[i].getTariffEndDate() != null) {
				endDate = formatter.format(accountArray[i].getTariffEndDate());
			}

			out.write("<td>" + startDate + "</td>");
			out.write("<td>" + endDate + "</td>");

			if (accountArray[i].isBanStatus()) {
				out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.yes") + "</td>");
				writeBanForm(out, accountArray[i], LocaleSupport.getLocalizedMessage(pageContext, "local.removeban"));

			} else {
				out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.no") + "</td>");
				writeBanForm(out, accountArray[i], LocaleSupport.getLocalizedMessage(pageContext, "local.makeban"));
			}

			out.write("</tr>");
		}
		out.write("</tbody>");
		out.write("</table>");

		writePagination("review_accounts");
	}

	private void writeClientAccountsTable(Set<Account> userAccounts) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();

		String userLogin = (String) session.getAttribute("login");

		Account[] userAccountsArray = userAccounts.toArray(new Account[userAccounts.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.accountslist") + "</caption>");
		out.write("<thead>");
		out.write("<tr>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.accountnumber") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.balance") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.unusedtraffic") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.currenttariffplan") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffsubscribedate") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffenddate") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.ban") + "</th>");
		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");
			out.write("<td>" + userAccountsArray[i].getAccountNumber() + "</td>");
			out.write("<td>" + userAccountsArray[i].getBalance() + "</td>");
			out.write("<td>" + userAccountsArray[i].getUnusedTraffic() + "</td>");
			out.write("<td>" + userAccountsArray[i].getTariffName() + "</td>");

			String startDate = null;
			if (userAccountsArray[i].getTariffStartDate() != null) {
				startDate = formatter.format(userAccountsArray[i].getTariffStartDate());
			}

			String endDate = null;
			if (userAccountsArray[i].getTariffEndDate() != null) {
				endDate = formatter.format(userAccountsArray[i].getTariffEndDate());
			}

			out.write("<td>" + startDate + "</td>");
			out.write("<td>" + endDate + "</td>");

			if (userAccountsArray[i].isBanStatus()) {
				out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.yes") + "</td>");
			} else {
				out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.no") + "</td>");
			}
			out.write("</tr>");
		}
		out.write("</tbody>");
		out.write("</table>");
		out.write("<br/>");

		writeAccountCreatingForm(out, userLogin);

		writePagination("review_account");

	}

	private void writePaymentForm(Set<Account> userAccounts) throws IOException {

		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();

		String userLogin = (String) session.getAttribute("login");

		Account[] userAccountsArray = userAccounts.toArray(new Account[userAccounts.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable accounttable payment\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.accountrefill") + "</caption>");
		out.write("<thead>");
		out.write("<tr>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.accountnumber") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.balance") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.paymentsum") + "</th>");
		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");
			out.write("<td>" + userAccountsArray[i].getAccountNumber() + "</td>");
			out.write("<td>" + userAccountsArray[i].getBalance() + "</td>");

			writePaymentForm(out, userAccountsArray[i], userLogin);

			out.write("</tr>");
		}
		out.write("</tbody>");
		out.write("</table>");

		writePagination("refill_account");
	}

	private void writeTariffCreatingForm() throws IOException {

		JspWriter out = pageContext.getOut();

		out.write("<form name=\"newTar\" action=\"InternetProviderController\" method=\"post\""
				+ " onsubmit=\"return validateCreatingTariffForm()\" >");
		out.write("<input type=\"hidden\" name=\"command\" value=\"form_tariff\" />");

		out.write("<table border=\"1\" class=\"informtable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.tariffcreating") + "</caption>");

		out.write("<tr>");
		out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.name") + "</td>");
		out.write("<td><input type=\"text\" name=\"tariffname\" value=\"\" /></td>");
		out.write("<td><span id=\"errName\" class=\"errMessage\"></span></td>");
		out.write("</tr>");

		out.write("<tr>");
		out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.charge") + "</td>");
		out.write("<td><input type=\"text\" name=\"charge\" value=\"\" /></td>");
		out.write("<td><span id=\"errCharge\" class=\"errMessage\"></span></td>");
		out.write("</tr>");

		out.write("<tr>");
		out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.transferrate") + "</td>");
		out.write("<td><input type=\"text\" name=\"internetrate\" value=\"\" /></td>");
		out.write("<td><span id=\"errRate\" class=\"errMessage\"></span></td>");
		out.write("</tr>");

		out.write("<tr>");
		out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.trafficvolume") + "</td>");
		out.write("<td><input type=\"text\" name=\"trafficvolume\" value=\"\" /></td>");
		out.write("<td><span id=\"errVolume\" class=\"errMessage\"></span></td>");
		out.write("</tr>");

		out.write("<tr>");
		out.write("<td>" + LocaleSupport.getLocalizedMessage(pageContext, "local.billingperiod") + "</td>");
		out.write("<td><input type=\"text\" name=\"billingperiod\" value=\"\" /></td>");
		out.write("<td><span id=\"errBilling\" class=\"errMessage\"></span></td>");
		out.write("</tr>");

		out.write("</table>");

		out.write("<input type=\"submit\" value=\""
				+ LocaleSupport.getLocalizedMessage(pageContext, "local.createtariffsubmit") + "\">");
		out.write("</form>");

	}

	private void writeBanForm(JspWriter out, Account account, String submitText) throws IOException {

		out.print("<td>");
		out.print("<form action=\"InternetProviderController\" method=\"post\">");
		out.print("<input type=\"hidden\" name=\"command\" value=\"change_ban\" /> ");
		out.print("<input type=\"hidden\" name=\"login\" value=\"" + account.getUser().getLogin() + "\" />");
		out.print("<input type=\"hidden\" name=\"accountnumber\" value=\"" + account.getAccountNumber() + "\" />");
		out.print("<input type=\"hidden\" name=\"currentban\" value=\"" + account.isBanStatus() + "\" />");
		out.print("<input type=\"submit\" value=\"" + submitText + "\" />");
		out.print("</form>");
		out.print("</td>");

	}

	private void writePaymentForm(JspWriter out, Account account, String userLogin) throws IOException {

		out.print("<td>");
		out.print("<form action=\"InternetProviderController\" method=\"post\" >");
		out.print("<input type=\"hidden\" name=\"command\" value=\"enhance_balance\" /> ");
		out.print("<input type=\"hidden\" name=\"login\" value=\"" + userLogin + "\" />");
		out.print("<input type=\"hidden\" name=\"accountnumber\" value=\"" + account.getAccountNumber() + "\" />");
		out.write("<input type=\"text\" name=\"refill\" value=\"\" pattern=\"[0-9]*\\.?[0-9]{2}\"" + " title=\""
				+ LocaleSupport.getLocalizedMessage(pageContext, "local.refillvalidmessage") + "\" />  у.е. ");
		out.print("<input type=\"submit\" value=\""
				+ LocaleSupport.getLocalizedMessage(pageContext, "local.refillaccount") + "\" />");
		out.print("</form>");
		out.print("</td>");

	}

	private void writeAccountCreatingForm(JspWriter out, String userLogin) throws IOException {

		out.print("<form action=\"InternetProviderController\" method=\"post\">");
		out.print("<input type=\"hidden\" name=\"command\" value=\"create_account\" /> ");
		out.print("<input type=\"hidden\" name=\"login\" value=\"" + userLogin + "\" />");
		out.print("<input type=\"submit\" value=\""
				+ LocaleSupport.getLocalizedMessage(pageContext, "local.createaccount") + "\" />");
		out.print("</form>");

	}

	private void writeAccountChoiceForm(Set<Account> userAccounts) throws IOException {

		JspWriter out = pageContext.getOut();

		out.write("<br/><br/>");
		for (Account account : userAccounts) {

			out.write("<div class=\"form\">");
			out.write("<form action=\"InternetProviderController\" method=\"post\">");
			out.write("<input type=\"hidden\" name=\"command\" value=\"show_choice_form\" /> ");
			out.write("<input type=\"hidden\" name=\"accountnumber\" value=\"" + account.getAccountNumber() + "\" /> ");
			out.write("<div class=\"submit\">");
			out.write("<input type=\"submit\" value=\"" + account.getAccountNumber() + " "
					+ LocaleSupport.getLocalizedMessage(pageContext, "local.account") + "\" />");
			out.write("</div>");
			out.write("</form>");
			out.write("</div>");

		}

	}

	private void writeTariffChoiceForm(Account userAccount, Set<Account> userAccounts, Set<Tariff> tariffs)
			throws IOException {

		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();

		writeAccountChoiceForm(userAccounts);

		out.write("<br/><br/> " + LocaleSupport.getLocalizedMessage(pageContext, "local.currentaccounttariff") + ": "
				+ userAccount.getTariffName());
		out.write("<br/> " + LocaleSupport.getLocalizedMessage(pageContext, "local.chooseoption") + ": <br/>");
		out.write("<div id=\"tariffchoice\">");
		out.write("<form action=\"InternetProviderController\" method=\"post\">");
		out.print("<input type=\"hidden\" name=\"command\" value=\"register_tariff_choice\" /> ");
		out.print("<input type=\"hidden\" name=\"login\" value=\"" + session.getAttribute("login") + "\" /> ");
		out.write("<input type=\"hidden\" name=\"accountnumber\" value=\"" + userAccount.getAccountNumber() + "\" /> ");

		for (Tariff tariff : tariffs) {
			if (!tariff.getName().equals(userAccount.getTariffName())
					& tariff.getCharge() <= userAccount.getBalance()) {
				out.write("<input type=\"radio\" name=\"tariffname\" value=\"" + tariff.getName() + "\" /> "
						+ tariff.getName() + " <br/> ");
			}

		}
		out.print("<input type=\"submit\" value=\" " + LocaleSupport.getLocalizedMessage(pageContext, "local.choose")
				+ " \" />");
		out.write("</form>");
		out.write("</div>");

	}

	private void writeRequestsForm(Set<ClientRequest> requests) throws IOException {

		JspWriter out = pageContext.getOut();
		HttpSession session = pageContext.getSession();

		ClientRequest[] requestsArray = requests.toArray(new ClientRequest[requests.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable accounttable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.clientrequests") + "</caption>");

		out.write("<thead>");
		out.write("<tr>");

		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.login") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.accountnumber") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.requestedtariff") + "</th>");
		out.write("<th></th>");

		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");

			out.write("<td>" + requestsArray[i].getUserLogin() + "</td>");
			out.write("<td>" + requestsArray[i].getAccountNumber() + "</td>");
			out.write("<td>" + requestsArray[i].getTariffName() + "</td>");

			out.write("<td>");
			writeRequestAnswerForm(requestsArray[i], LocaleSupport.getLocalizedMessage(pageContext, "local.accept"));
			writeRequestAnswerForm(requestsArray[i], LocaleSupport.getLocalizedMessage(pageContext, "local.deny"));
			out.write("</td>");

			out.write("</tr>");
		}

		out.write("</tbody>");
		out.write("</table>");

		writePagination("show_tariffchange_form");
	}

	private void writeClientRequestsForm(Set<ClientRequest> userRequests) throws IOException {

		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();

		ClientRequest[] userRequestsArray = userRequests.toArray(new ClientRequest[userRequests.size()]);

		Integer[] startEndElementPositions = (Integer[]) session.getAttribute("startEndPages");
		int startElement = startEndElementPositions[0];
		int endElement = startEndElementPositions[1];

		out.write("<table border=\"1\" class=\"informtable accounttable\">");
		out.write("<caption>" + LocaleSupport.getLocalizedMessage(pageContext, "local.sentrequests") + "</caption>");

		out.write("<thead>");
		out.write("<tr>");

		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.login") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.accountnumber") + "</th>");
		out.write("<th>" + LocaleSupport.getLocalizedMessage(pageContext, "local.requestedtariff") + "</th>");
		out.write("<th></th>");

		out.write("</tr>");
		out.write("</thead>");
		out.write("<tbody>");

		for (int i = startElement; i < endElement; i++) {
			out.write("<tr>");

			out.write("<td>" + userRequestsArray[i].getUserLogin() + "</td>");
			out.write("<td>" + userRequestsArray[i].getAccountNumber() + "</td>");
			out.write("<td>" + userRequestsArray[i].getTariffName() + "</td>");

			out.write("<td>");
			writeRequestCancelForm(userRequestsArray[i]);
			out.write("</td>");

			out.write("</tr>");
		}
		out.write("</tbody>");
		out.write("</table>");

		writePagination("show_sent_requests");
	}

	private void writeRequestAnswerForm(ClientRequest userRequest, String adminAction) throws IOException {

		JspWriter out = pageContext.getOut();

		out.write("<div class=\"form\">");
		out.print("<form action=\"InternetProviderController\" method=\"post\">");
		out.print("<input type=\"hidden\" name=\"command\" value=\"change_usertariff\" /> ");
		out.print("<input type=\"hidden\" name=\"adminaction\" value=\"" + adminAction + "\" />");
		out.print("<input type=\"hidden\" name=\"login\" value=\"" + userRequest.getUserLogin() + "\" />");
		out.print("<input type=\"hidden\" name=\"accountnumber\" value=\"" + userRequest.getAccountNumber() + "\" />");
		out.print("<input type=\"hidden\" name=\"newtariffname\" value=\"" + userRequest.getTariffName() + "\" />");
		out.print("<input type=\"submit\" value=\"" + adminAction + "\" />");
		out.print("</form>");
		out.write("</div>");

	}

	private void writeRequestCancelForm(ClientRequest userRequest) throws IOException {

		JspWriter out = pageContext.getOut();

		out.write("<div class=\"form\">");
		out.print("<form action=\"InternetProviderController\" method=\"post\">");
		out.print("<input type=\"hidden\" name=\"command\" value=\"cancel_request\" /> ");
		out.print("<input type=\"hidden\" name=\"login\" value=\"" + userRequest.getUserLogin() + "\" />");
		out.print("<input type=\"hidden\" name=\"accountnumber\" value=\"" + userRequest.getAccountNumber() + "\" />");
		out.print("<input type=\"hidden\" name=\"newtariffname\" value=\"" + userRequest.getTariffName() + "\" />");
		out.print("<input type=\"submit\" value=\"" + LocaleSupport.getLocalizedMessage(pageContext, "local.cancel")
				+ "\" />");
		out.print("</form>");
		out.write("</div>");

	}

	private void writePagination(String commandName) throws IOException {

		JspWriter out = pageContext.getOut();
		HttpSession session = pageContext.getSession();

		List<Integer> pagesCount = (List<Integer>) session.getAttribute("pages");

		out.print("<br/><br/>");
		for (Integer count : pagesCount) {
			out.print("<form style=\"display: inline-block\" action=\"InternetProviderController\" method=\"post\">");
			out.print("<input type=\"hidden\" name=\"command\" value=\"" + commandName + "\" /> ");
			out.print("<input type=\"hidden\" name=\"pageNumber\" value=" + count + " /> ");
			out.print("<input type=\"submit\" value=\"" + LocaleSupport.getLocalizedMessage(pageContext, "local.page")
					+ " " + count + "\" />");
			out.print("</form>");
		}

		session.removeAttribute("pageNumber");
		session.removeAttribute("pages");
		session.removeAttribute("startEndPages");
	}
}
