package by.internetprovider.controller.command.implementation.account;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.internetprovider.controller.command.Command;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for creating a new client account.
 * 
 * @author Pavel Pranovich
 *
 */
public class AccountCreating implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException, NamingException {

		String login = request.getParameter("login");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		AccountService accountServiceImpl = serviceFactory.getAccountServiceImpl();

		accountServiceImpl.createClientAccount(login);

		ClientAccountOverview userAccount = new ClientAccountOverview();
		userAccount.execute(request, response);

	}

}
