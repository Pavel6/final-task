package by.internetprovider.service;

import java.util.Set;

import by.internetprovider.bean.Tariff;
import by.internetprovider.service.exception.ServiceException;

/**
 * This interface defines operations with tariff plans on the Service Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public interface TariffService {

	/**
	 * Returns all the current tariff plans.
	 * 
	 * @return tariff plans.
	 * @throws ServiceException
	 */
	public Set<Tariff> getTariffs() throws ServiceException;

	/**
	 * Creates a new tariff.
	 * 
	 * @param tariffName
	 *            - tariff plan name.
	 * @param userCharge
	 *            - charge, coin of account.
	 * @param internetRate
	 *            - data transfer rate, Mbit/sec.
	 * @param trafficVolume
	 *            - traffic volume (Gb).
	 * @param billingPeriod
	 *            - number of days of the billing period.
	 * @return <code>true</code> if the input parameters meet the validation
	 *         requirements, <code>false</ code> - otherwise.
	 * @throws ServiceException
	 */
	public boolean createTariff(String tariffName, String userCharge, String internetRate, String trafficVolume,
			String billingPeriod) throws ServiceException;

}
