package by.internetprovider.service;

import java.util.Set;

import by.internetprovider.bean.Account;
import by.internetprovider.service.exception.ServiceException;

/**
 * This interface defines operations with elements of the accounts of clients on
 * the Service Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public interface AccountService {

	/**
	 * Returns the accounts of all clients.
	 * 
	 * @return accounts of clients.
	 * @throws ServiceException
	 */
	public Set<Account> getClientAccounts() throws ServiceException;

	/**
	 * Returns the accounts of the client with the specified login.
	 * 
	 * @param login
	 *            - client login.
	 * @return accounts of a client.
	 * @throws ServiceException
	 */
	public Set<Account> getClientAccounts(String login) throws ServiceException;

	/**
	 * Returns the account with the given login and the given account number.
	 * 
	 * @param login
	 *            - client login.
	 * @param accountNumber
	 *            - account number of the client.
	 * @return client account.
	 * @throws ServiceException
	 */
	public Account getClientAccount(String login, byte accountNumber) throws ServiceException;

	/**
	 * Creates a new account for a client with the specified login.
	 * 
	 * @param login
	 *            - client login.
	 * @throws ServiceException
	 */
	public void createClientAccount(String login) throws ServiceException;

	/**
	 * Changes the ban status on a particular account of the client with the
	 * specified login.
	 * 
	 * @param login
	 *            - client login.
	 * @param accountNumber
	 *            - account number of the client.
	 * @param currentBan
	 *            - current ban status.
	 * @throws ServiceException
	 */
	public void changeBan(String login, byte accountNumber, boolean currentBan) throws ServiceException;

	/**
	 * Refills the balance (the amount of payment means) by a certain amount on
	 * the specified account of the client with the given login.
	 * 
	 * @param login
	 *            - client login.
	 * @param accountNumber
	 *            - account number of the client.
	 * @param refill
	 *            - amount of replenished means of payment.
	 * @return <code>true</code> if the value <code>refill</code> meets the
	 *         validation requirements, <code>false</ code> - otherwise.
	 * @throws ServiceException
	 */
	public boolean raiseBalance(String login, byte accountNumber, String refill) throws ServiceException;

}
