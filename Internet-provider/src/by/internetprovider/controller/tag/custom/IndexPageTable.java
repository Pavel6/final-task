package by.internetprovider.controller.tag.custom;

import java.io.IOException;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.internetprovider.bean.Tariff;
import by.internetprovider.controller.InternetProviderController;
import by.internetprovider.controller.pagination.Pagination;
import by.internetprovider.service.TariffService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is a handler for a custom tag &lt;indexpagetable /&gt;.
 * 
 * @author Pavel Pranovich
 *
 */
public class IndexPageTable extends TagSupport {

	private static Logger logger = Logger.getLogger(InternetProviderController.class.getName());

	private static final long serialVersionUID = 1L;

	@Override
	public int doStartTag() {

		HttpSession session = pageContext.getSession();
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		TariffService tariffServiceImpl = serviceFactory.getTariffServiceImpl();

		try {
			Set<Tariff> tariffs = tariffServiceImpl.getTariffs();

			Pagination.setArrayPagination(tariffs.toArray(), 5, null, session);

			session.setAttribute("userAction", "showTariffs");
			session.setAttribute("tariffs", tariffs);
			session.setAttribute("lastCommandName", "review_tariffs");
		} catch (ServiceException e) {
			logger.info(e.getCause());

			RequestDispatcher dispatch = request.getRequestDispatcher("/page/error.jsp");
			try {
				dispatch.forward(request, response);
			} catch (ServletException | IOException e1) {
				logger.info(e1.getCause());
			}
		}

		return SKIP_BODY;
	}

	@Override
	public int doEndTag() {
		return EVAL_PAGE;
	}

}
