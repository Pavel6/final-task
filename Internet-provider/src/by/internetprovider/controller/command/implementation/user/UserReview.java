package by.internetprovider.controller.command.implementation.user;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.User;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.pagination.Pagination;
import by.internetprovider.service.UserService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to represent the administrator with the overview of all users
 * (administrators and clients).
 * 
 * @author Pavel Pranovich
 *
 */
public class UserReview implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		HttpSession session = request.getSession(true);

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		UserService userServiceImpl = serviceFactory.getUserServiceImpl();
		Set<User> users = userServiceImpl.getUsers();

		Pagination.setArrayPagination(users.toArray(), 5, request, session);

		session.setAttribute("userAction", "showUsers");
		session.setAttribute("users", users);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
