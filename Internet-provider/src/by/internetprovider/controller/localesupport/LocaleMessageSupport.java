package by.internetprovider.controller.localesupport;

import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * A class is used to obtain local-specific values for given keys.
 * 
 * @author Pavel Pranovich
 *
 */
public class LocaleMessageSupport {

	/**
	 * Returns the object <code>ResourceBundle</code> for a specific locale
	 * stored as an attribute of <code>HttpSession</code>.
	 * 
	 * @param request
	 *            - Request object
	 * @return <code>ResourceBundle</code> object.
	 */
	private static ResourceBundle getLocalBundle(HttpServletRequest request) {

		HttpSession session = request.getSession(true);
		String local = (String) session.getAttribute("local");

		ResourceBundle bundle = ResourceBundle.getBundle("by.internetprovider.localization.local", new Locale(local));

		return bundle;
	}

	/**
	 * Returns a locale-specific value for a given key.
	 * 
	 * @param request
	 *            - Request object
	 * @param key
	 *            - key
	 * @return the value of pairs "key"-"value".
	 */
	public static String getLocalizedMessage(HttpServletRequest request, String key) {

		ResourceBundle bundle = getLocalBundle(request);
		String value = bundle.getString(key);

		return value;
	}

}
