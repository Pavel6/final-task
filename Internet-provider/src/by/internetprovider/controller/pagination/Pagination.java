package by.internetprovider.controller.pagination;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class is intended for viewing “long lists” in the page-by mode.
 * 
 * @author Pavel Pranovich
 *
 */
public class Pagination {

	/**
	 * Sets the partitioning of the array elements of "long lists" into pages.
	 * 
	 * @param collArray
	 *            - an array corresponding to a collection of "long list".
	 * @param maxEntriesPerPage
	 *            - maximum number of elements per page.
	 * @param request
	 *            - Request object.
	 * @param session
	 *            - current session.
	 */
	public static void setArrayPagination(Object[] collArray, int maxEntriesPerPage, HttpServletRequest request,
			HttpSession session) {

		String pageNumber = null;

		if (request != null) {
			pageNumber = request.getParameter("pageNumber");
			session.setAttribute("pageNumber", pageNumber);
		}

		session.setAttribute("pages", getPagesCount(collArray, maxEntriesPerPage));
		session.setAttribute("startEndPages", getStartEndElementByOffset(collArray, maxEntriesPerPage, session));

	}

	/**
	 * Returns the value of the offset of elements from the beginning of “long
	 * list” for displaying the requested page.
	 * 
	 * @param maxEntriesPerPage
	 *            - maximum number of elements per page.
	 * @param session
	 *            - current session.
	 * @return value of the offset of elements.
	 */
	private static int getOffset(int maxEntriesPerPage, HttpSession session) {
		int page = 1;

		String pageNumber = (String) session.getAttribute("pageNumber");

		if (pageNumber != null) {
			page = Integer.parseInt(pageNumber);
		}

		int offset = maxEntriesPerPage * (page - 1);

		return offset;
	}

	/**
	 * Returns a list of object-wrappers, corresponding to the pages for “long
	 * list”.
	 * 
	 * @param collArray
	 *            - an array corresponding to a collection of "long list".
	 * @param maxEntriesPerPage
	 *            - maximum number of elements per page.
	 * @return list of <code>Integer</code> objects.
	 */
	private static List<Integer> getPagesCount(Object[] collArray, int maxEntriesPerPage) {

		int collSize = collArray.length;
		int pages = collSize / maxEntriesPerPage;

		if (collSize % maxEntriesPerPage != 0) {
			pages = pages + 1;
		}

		List<Integer> pageNumbers = new ArrayList<Integer>();

		for (int i = 1; i <= pages; i++) {
			pageNumbers.add(new Integer(i));
		}

		return pageNumbers;
	}

	/**
	 * Returns a two component array of sequence numbers of the first and the
	 * last element of a particular page of “long list”.
	 * 
	 * @param collArray
	 *            - an array corresponding to a collection of "long list".
	 * @param maxEntriesPerPage
	 *            - maximum number of elements per page.
	 * @param session
	 *            - current session.
	 * @return
	 */
	private static Integer[] getStartEndElementByOffset(Object[] collArray, int maxEntriesPerPage,
			HttpSession session) {

		int offset = getOffset(maxEntriesPerPage, session);

		int to = offset + maxEntriesPerPage;
		int collSize = collArray.length;

		if (offset > collSize) {
			offset = collSize;
		}

		if (to > collSize) {
			to = collSize;
		}

		Integer[] startEndPages = new Integer[2];

		startEndPages[0] = offset;
		startEndPages[1] = to;

		return startEndPages;

	}

}
