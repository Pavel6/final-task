package by.internetprovider.service.implementation;

import java.util.Set;

import by.internetprovider.bean.RegFormCheckout;
import by.internetprovider.bean.RegUser;
import by.internetprovider.bean.User;
import by.internetprovider.dao.UserDao;
import by.internetprovider.dao.exception.DaoException;
import by.internetprovider.dao.factory.DaoFactory;
import by.internetprovider.service.UserService;
import by.internetprovider.service.exception.ServiceException;

/**
 * Class that implements operations with users (administrators and clients) on
 * the Service Layer.
 * 
 * 
 * @author Pavel Pranovich
 *
 */
public class UserServiceImpl implements UserService {

	@Override
	public RegFormCheckout registrateClient(RegUser regUser) throws ServiceException {

		boolean isValidatedRegUser = validateRegUser(regUser);

		boolean isLoginFree = false;
		if (isValidatedRegUser) {
			isLoginFree = performRegistration(regUser);
		}

		RegFormCheckout checkout = new RegFormCheckout(isLoginFree, isValidatedRegUser);

		return checkout;
	}

	@Override
	public User signIn(String login, String password) throws ServiceException {

		User user = new User();

		if (login.isEmpty() || password.isEmpty()) {
			user.setName("");
			user.setSurname("");
			user.setRoleName("");
			return user;
		}

		UserDao userDaoImpl = getTariffDaoImpl();

		try {
			user = userDaoImpl.signIn(login, password);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return user;
	}

	@Override
	public Set<User> getUsers() throws ServiceException {

		UserDao userDaoImpl = getTariffDaoImpl();

		Set<User> users;

		try {
			users = userDaoImpl.getUsers();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return users;
	}

	/**
	 * 
	 * Carries out the validation of the input values for registration of the
	 * specified new client.
	 * 
	 * @param newClient
	 *            - new client.
	 * @return <code>true</code> if the <code>RegUser</code> object meets the
	 *         validation requirements, <code>false</ code> - otherwise.
	 * @see RegUser
	 */
	public boolean validateRegUser(RegUser newClient) {
		boolean isValidated = true;

		String REGEXNAME = "^[A-Z]{1}[a-zA-Z ]{1,10}$";
		String REGEXSURNAME = "^[A-Z]{1}[a-zA-Z ]{1,15}$";
		String REGEXLOGIN = "\\w{5,}";
		String REGEXPASSWORD = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d$@$!%*?&]{6,}$";

		String name = newClient.getName();
		String surname = newClient.getSurname();
		String login = newClient.getLogin();
		String password = newClient.getPassword();

		if (name.isEmpty() || surname.isEmpty() || login.isEmpty() || password.isEmpty()) {
			isValidated = false;
		}

		if (!name.matches(REGEXNAME) || !surname.matches(REGEXSURNAME) || !login.matches(REGEXLOGIN)
				|| !password.matches(REGEXPASSWORD)) {
			isValidated = false;
		}

		return isValidated;
	}

	/**
	 * Registers the specified new client.
	 * <p>
	 * This operation is carried out in the case when the input data meet the
	 * validation requirements.
	 * 
	 * @param newClient
	 *            - new client.
	 * @return <code>true</code> if login of <code>RegUser</code> doesn't exist
	 *         in DataBase, <code>false</code> if login of <code>RegUser</code>
	 *         exists in DataBase.
	 * @throws ServiceException
	 */
	private boolean performRegistration(RegUser newClient) throws ServiceException {
		boolean isLoginFree = true;

		DaoFactory daoFactory = DaoFactory.getInstance();
		UserDao userDaoIml = daoFactory.getUserDaoImpl();

		try {
			isLoginFree = userDaoIml.registrateClient(newClient);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return isLoginFree;
	}

	/*
	 * Auxiliary method for obtaining a reference to an object whose class
	 * implements the interface UserDao.
	 */
	private UserDao getTariffDaoImpl() {

		DaoFactory daoFactory = DaoFactory.getInstance();
		UserDao userDaoImpl = daoFactory.getUserDaoImpl();

		return userDaoImpl;
	}

}
