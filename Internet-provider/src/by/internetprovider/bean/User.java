package by.internetprovider.bean;

import java.io.Serializable;

/**
 * Class for an user.
 * 
 * @author Pavel Pranovich
 *
 */

public class User implements Comparable<User>, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * User name.
	 */
	private String name;

	/**
	 * User surname.
	 */
	private String surname;

	/**
	 * User login.
	 */
	private String login;

	/**
	 * User role.
	 */
	private String roleName;

	/**
	 * Creates a new user.
	 */
	public User() {
		super();
	}

	/**
	 * Creates a new user for the specified <code>name</code>,
	 * <code>surname</code>,<code>login</code> and <code>roleName</code>.
	 * 
	 * @param name
	 *            - name.
	 * @param surname
	 *            - surname.
	 * @param login
	 *            - login.
	 * @param roleName
	 *            - user role name.
	 */
	public User(String name, String surname, String login, String roleName) {
		super();
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.roleName = roleName;
	}

	/**
	 * Creates a new user for the specified <code>name</code>,
	 * <code>surname</code> and <code>login</code>.
	 * 
	 * @param name
	 *            - name.
	 * @param surname
	 *            - surname.
	 * @param login
	 *            - login.
	 */
	public User(String name, String surname, String login) {
		super();
		this.name = name;
		this.surname = surname;
		this.login = login;
	}

	/**
	 * Creates a new user for the specified <code>name</code> and
	 * <code>surname</code>.
	 * 
	 * @param name
	 *            - name.
	 * @param surname
	 *            - surname.
	 */
	public User(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}

	/**
	 * Creates a new user for the specified <code>login</code>.
	 * 
	 * @param login
	 *            - login.
	 */
	public User(String login) {
		super();
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getLogin() {
		return login;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((roleName == null) ? 0 : roleName.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (roleName == null) {
			if (other.roleName != null)
				return false;
		} else if (!roleName.equals(other.roleName))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", surname=" + surname + ", login=" + login + ", roleName=" + roleName + "]";
	}

	@Override
	public int compareTo(User otherUser) {

		if (this.surname.compareTo(otherUser.surname) == 0) {
			if (this.name.compareTo(otherUser.name) == 0) {
				return this.login.compareTo(otherUser.login);

			} else {
				return this.name.compareTo(otherUser.name);

			}
		} else {
			return this.surname.compareTo(otherUser.surname);

		}

	}

}
