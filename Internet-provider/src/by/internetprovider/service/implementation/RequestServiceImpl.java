package by.internetprovider.service.implementation;

import java.util.Set;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.dao.RequestDao;
import by.internetprovider.dao.exception.DaoException;
import by.internetprovider.dao.factory.DaoFactory;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.exception.ServiceException;

/**
 * Class that implements operations with client requests on the Service Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public class RequestServiceImpl implements RequestService {

	@Override
	public String registerClientRequest(String login, byte accountNumber, String tariffName) throws ServiceException {

		String answerMessage;

		if (tariffName == null) {
			answerMessage = "choose a tariff";
		} else {

			RequestDao requestDaoImpl = getRequestDaoImpl();

			try {
				requestDaoImpl.registerClientRequest(login, accountNumber, tariffName);
			} catch (DaoException e) {
				throw new ServiceException(e);
			}

			answerMessage = "requestSent";
		}

		return answerMessage;
	}

	@Override
	public Set<ClientRequest> getRequests() throws ServiceException {

		RequestDao requestDaoImpl = getRequestDaoImpl();

		Set<ClientRequest> userRequests;

		try {
			userRequests = requestDaoImpl.getRequests();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return userRequests;
	}

	@Override
	public Set<ClientRequest> getClientRequests(String userLogin) throws ServiceException {

		RequestDao requestDaoImpl = getRequestDaoImpl();

		Set<ClientRequest> userRequests;

		try {
			userRequests = requestDaoImpl.getRequests(userLogin);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return userRequests;
	}

	@Override
	public void performClientRequest(ClientRequest userRequest, String adminAction) throws ServiceException {

		RequestDao requestDaoImpl = getRequestDaoImpl();

		try {

			if (adminAction.equals("accept")) {
				requestDaoImpl.performAcceptedClientRequest(userRequest);
			} else if (adminAction.equals("deny")) {
				requestDaoImpl.deleteRequest(userRequest);
			}

		} catch (DaoException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void cancelRequest(ClientRequest clientRequest) throws ServiceException {

		RequestDao requestDaoImpl = getRequestDaoImpl();

		try {

			requestDaoImpl.cancelClientRequest(clientRequest);

		} catch (DaoException e) {
			throw new ServiceException(e);
		}

	}

	/*
	 * Auxiliary method for obtaining a reference to an object whose class
	 * implements the interface RequestDao.
	 */
	private RequestDao getRequestDaoImpl() {

		DaoFactory daoFactory = DaoFactory.getInstance();
		RequestDao requestDaoImpl = daoFactory.getRequestDaoImpl();

		return requestDaoImpl;
	}

}
