<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="custom" uri="/WEB-INF/tld/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Internet-provider</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/style/css/internet-provider.css">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.internetprovider.localization.local"
	var="loc" />

<fmt:message key="local.en" bundle="${loc}" var="en_button" />
<fmt:message key="local.ru" bundle="${loc}" var="ru_button" />
<fmt:message key="local.intprovider" bundle="${loc}" var="intprovider" />
<fmt:message key="local.login" bundle="${loc}" var="login" />
<fmt:message key="local.signout" bundle="${loc}" var="signout" />
<fmt:message key="local.action" bundle="${loc}" var="action" />
<fmt:message key="local.news" bundle="${loc}" var="news" />
<fmt:message key="local.asideaction" bundle="${loc}" var="asideact" />
<fmt:message key="local.asidenewsvacancy" bundle="${loc}" var="asidenewsvac" />
<fmt:message key="local.asidenewstariff" bundle="${loc}" var="asidenewstar" />
<fmt:message key="local.tariffplans" bundle="${loc}" var="tariffplans" />
<fmt:message key="local.user" bundle="${loc}" var="user" />
<fmt:message key="local.userpage" bundle="${loc}" var="userpage" />
<fmt:message key="local.viewtariffs" bundle="${loc}" var="viewtariffs" />
<fmt:message key="local.viewusers" bundle="${loc}" var="viewusers" />
<fmt:message key="local.viewaccounts" bundle="${loc}" var="viewaccs" />
<fmt:message key="local.createtariff" bundle="${loc}" var="createtar" />
<fmt:message key="local.changeusertariff" bundle="${loc}" var="changeusertar" />
<fmt:message key="local.username" bundle="${loc}" var="username" />
<fmt:message key="local.usersurname" bundle="${loc}" var="usersurname" />
<fmt:message key="local.role" bundle="${loc}" var="role" />

</head>
<body>
	<div id="menu-local">
		<table>
			<tr>
				<td>
					<form action="InternetProviderController" method="post">
						<input type="hidden" name="command" value="localise" /> 
						<input type="hidden" name="local" value="en" /> 
						<input type="hidden" name="text" value="localization.en" /> 
						<input type="submit" value="${en_button}" />
					</form>
				</td>

				<td>
					<form action="InternetProviderController" method="post">
						<input type="hidden" name="command" value="localise" /> 
						<input type="hidden" name="local" value="ru" /> 
						<input type="hidden" name="text" value="localization.ru" /> 
						<input type="submit" value="${ru_button}" />
					</form>
				</td>
			</tr>
		</table>
	</div>

	<header>
	     <h1>${intprovider} "LightWave telecom"</h1>
	</header>

	<div class="wrapper">
		<nav>
		    <div class="person">
			    <h3 class="title">${user}:</h3>
			    
			    <hr>
                <label>${username}:</label>
		    	<c:out value="${sessionScope.name}" /><br/>
					
		    	<label>${usersurname}:</label>
			    <c:out value="${sessionScope.surname}" /><br/>
					
			    <label>${login}:</label>
		    	<c:out value="${sessionScope.login}" /><br/>
					
			    <label>${role}:</label>
			    <c:out value="${sessionScope.role}" /><br/>

			    <div class="form">
				    <form action="InternetProviderController" method="post">
				    	<input type="hidden" name="command" value="signout" />
					
					    <div class="submit">
					    	<input type="submit" value="${signout}" />
				    	</div>
				    </form>
			    </div>
		    </div>
		</nav>

		<section>
		    <h2>${userpage}</h2>

		    <hr>
		    <custom:userform 
		        command="review_tariffs"  submitValue="${viewtariffs}" /> 
		
		    <custom:userform
			    command="review_users" submitValue="${viewusers}" /> 
			
		    <custom:userform
			    command="review_accounts" submitValue="${viewaccs}" /> 
			
		    <custom:userform
		    	command="show_tariff_form" submitValue="${createtar}" /> 
			
		    <custom:userform
			    command="show_tariffchange_form" submitValue="${changeusertar}" /> 
						
		    <custom:informwindow /> 
		    <custom:formtariffmessage />		
		</section>

		<aside>
		    <div class="aside">
			    <h2 class="title">${action}:</h2>
			    
			    <hr>		
			    <p class="paragraph">${asideact}.</p>
		    </div>
		
		    <div class="aside">
		    	<h2 class="title">${news}:</h2>
		    	
			    <hr>			
		    	<ul>
				    <li>${asidenewsvac}</li>
			    	<li>${asidenewstar}</li>
			    </ul>
		    </div>
		</aside>
	</div>

	<footer>
	    <p>"LightWave telecom" 2017</p>
	</footer>
	
	<script src="${pageContext.request.contextPath}/javascript/newtariff.jsp"></script>
</body>
</html>