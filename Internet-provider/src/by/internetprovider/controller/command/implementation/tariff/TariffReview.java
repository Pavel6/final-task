package by.internetprovider.controller.command.implementation.tariff;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.Tariff;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.pagination.Pagination;
import by.internetprovider.service.TariffService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to represent administrators or clients with the overview of all
 * current tariff plans.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffReview implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServiceException {

		HttpSession session = request.getSession(true);

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		TariffService tariffServiceImpl = serviceFactory.getTariffServiceImpl();
		Set<Tariff> tariffs = tariffServiceImpl.getTariffs();

		Pagination.setArrayPagination(tariffs.toArray(), 5, request, session);

		session.setAttribute("userAction", "showTariffs");
		session.setAttribute("tariffs", tariffs);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
