package by.internetprovider.dao.implementation;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import by.internetprovider.bean.Tariff;
import by.internetprovider.dao.TariffDao;
import by.internetprovider.dao.exception.DaoException;

/**
 * Class that implements operations with tariff plans on the DAL (Data Access
 * Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffDaoImpl extends CommonDaoImpl implements TariffDao {

	@Override
	public Set<Tariff> getTariffs() throws DaoException {

		Set<Tariff> tariffs = new TreeSet<Tariff>();

		List<Map<String, String>> resultList = performSelectRequest("SELECT * FROM tariff_plans");

		for (Map<String, String> map : resultList) {
			String tariffName = map.get("t_p_name");
			float userCharge = Float.parseFloat(map.get("user_charge"));
			short internetSpeed = Short.parseShort(map.get("internet_speed"));
			byte trafficVolume = Byte.parseByte(map.get("traffic_volume"));
			byte billingPeriod = Byte.parseByte(map.get("billing_period"));

			Tariff tariff = new Tariff(tariffName, userCharge, internetSpeed, trafficVolume, billingPeriod);

			tariffs.add(tariff);
		}

		return tariffs;
	}

	@Override
	public void createTariff(Tariff newTariff) throws DaoException {

		performUpdateRequest("INSERT INTO tariff_plans (`t_p_name`, `user_charge`, `internet_speed`, `traffic_volume`,"
				+ " `billing_period`) VALUES" + " ('" + newTariff.getName() + "', '" + newTariff.getCharge() + "',"
				+ " '" + newTariff.getInternetRate() + "', '" + newTariff.getTrafficVolume() + "'," + " '"
				+ newTariff.getBillingPeriod() + "')");

	}

}
