package by.internetprovider.bean;

import java.io.Serializable;

/**
 * 
 * Class for results of validation of new client registration form.
 * 
 * <p>
 * The class is used for storing data on the compliance of input data with the
 * requirements of validation on the server as well as on determining the match
 * of the entered login with the logins in the database.
 * 
 * @author Pavel Pranovich
 *
 */
public class RegFormCheckout implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Indicator of absence of the entered login in database: true - no login in
	 * database, false - entered login exists in database.
	 */
	private boolean isLoginFree;

	/**
	 * Indicator of correspondence of the input data to validation requirements:
	 * true - the input data meet the requirements. false - the input data do
	 * not meet the requirements.
	 */
	private boolean isValidationPassed;

	/**
	 * Creates an instance for results of validation of the registration form.
	 */
	public RegFormCheckout() {
		super();
	}

	/**
	 * Creates an instance for results of validation of the registration form.
	 * 
	 * @param isLoginFree
	 *            - indicator of absence of the entered login in database.
	 * @param isValidationPassed
	 *            - indicator of correspondence of the input data to validation
	 *            requirements.
	 */
	public RegFormCheckout(boolean isLoginFree, boolean isValidationPassed) {
		super();
		this.isLoginFree = isLoginFree;
		this.isValidationPassed = isValidationPassed;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isLoginFree() {
		return isLoginFree;
	}

	public boolean isValidationPassed() {
		return isValidationPassed;
	}

	public void setLoginFree(boolean isLoginFree) {
		this.isLoginFree = isLoginFree;
	}

	public void setValidationPassed(boolean isValidationPassed) {
		this.isValidationPassed = isValidationPassed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isLoginFree ? 1231 : 1237);
		result = prime * result + (isValidationPassed ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegFormCheckout other = (RegFormCheckout) obj;
		if (isLoginFree != other.isLoginFree)
			return false;
		if (isValidationPassed != other.isValidationPassed)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RegFormCheckout [isLoginFree=" + isLoginFree + ", isValidationPassed=" + isValidationPassed + "]";
	}

}
