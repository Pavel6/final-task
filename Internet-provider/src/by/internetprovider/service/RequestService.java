package by.internetprovider.service;

import java.util.Set;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.bean.Tariff;
import by.internetprovider.service.exception.ServiceException;

/**
 * This interface defines operations with requests of clients on the Service
 * Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public interface RequestService {

	/**
	 * Registers a client request with a given login to connect the tariff plan.
	 * 
	 * @param clientLogin
	 *            - client login.
	 * @param accountNumber
	 *            - account number of the client.
	 * @param tariffName
	 *            - name of the new tariff plan chosen by the client.
	 * @return request state message.
	 * @throws ServiceException
	 * @see Tariff
	 */
	public String registerClientRequest(String clientLogin, byte accountNumber, String tariffName)
			throws ServiceException;

	/**
	 * Returns the requests of all clients.
	 * 
	 * @return client's requests.
	 * @throws ServiceException
	 */
	public Set<ClientRequest> getRequests() throws ServiceException;

	/**
	 * Returns the client's requests with the specified login.
	 * 
	 * @param clientLogin
	 *            - логин клиента.
	 * @return client's requests.
	 * @throws ServiceException
	 */
	public Set<ClientRequest> getClientRequests(String clientLogin) throws ServiceException;

	/**
	 * Executes the specified client request depending on the administrator's
	 * command.
	 * 
	 * @param clientRequest
	 *            - client's request.
	 * @param adminCommand
	 *            - administrator's command.
	 * @throws ServiceException
	 */
	public void performClientRequest(ClientRequest clientRequest, String adminCommand) throws ServiceException;

	/**
	 * Cancels the specified client request.
	 * 
	 * @param clientRequest
	 *            - client's request.
	 * @throws ServiceException
	 */
	public void cancelRequest(ClientRequest clientRequest) throws ServiceException;

}
