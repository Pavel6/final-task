package by.internetprovider.bean;

/**
 * Class for an user.
 * 
 * <p>
 * This class is subclass for <code>{@link User}</code>. The class is used for
 * registration of a new client in the system.
 * 
 * @author Pavel Pranovich
 *
 */

public class RegUser extends User {

	private static final long serialVersionUID = 1L;

	/**
	 * Password of the user (client).
	 */
	private String password;

	/**
	 * Client age.
	 */
	private byte age;

	/**
	 * Client e-mail.
	 */
	private String mail;

	/**
	 * Alternative client e-mail.
	 */
	private String mailAlt;

	/**
	 * Creates a new user with data for registration - name, surname, login,
	 * password, age, e-mail , alternative e-mail.
	 * 
	 * @param name
	 *            - client name.
	 * @param surname
	 *            - client surname.
	 * @param login
	 *            - login.
	 * @param password
	 *            - password.
	 * @param age
	 *            - client age.
	 * @param mail
	 *            - e-mail.
	 * @param mailAlt
	 *            - alternative e-mail.
	 */
	public RegUser(String name, String surname, String login, String password, byte age, String mail, String mailAlt) {
		super(name, surname, login);
		this.password = password;
		this.age = age;
		this.mail = mail;
		this.mailAlt = mailAlt;
	}

	public String getPassword() {
		return password;
	}

	public byte getAge() {
		return age;
	}

	public String getMail() {
		return mail;
	}

	public String getMailAlt() {
		return mailAlt;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAge(byte age) {
		this.age = age;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setMailAlt(String mailAlt) {
		this.mailAlt = mailAlt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + age;
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result + ((mailAlt == null) ? 0 : mailAlt.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegUser other = (RegUser) obj;
		if (age != other.age)
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (mailAlt == null) {
			if (other.mailAlt != null)
				return false;
		} else if (!mailAlt.equals(other.mailAlt))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RegUser [password=" + password + ", age=" + age + ", mail=" + mail + ", mailAlt=" + mailAlt + "]";
	}

}
