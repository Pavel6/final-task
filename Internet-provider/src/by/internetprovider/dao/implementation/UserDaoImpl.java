package by.internetprovider.dao.implementation;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import by.internetprovider.bean.RegUser;

import by.internetprovider.bean.User;
import by.internetprovider.dao.UserDao;
import by.internetprovider.dao.exception.DaoException;

/**
 * Class that implements operations with users (administrators and clients) on
 * the DAL (Data Access Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public class UserDaoImpl extends CommonDaoImpl implements UserDao {

	@Override
	public boolean registrateClient(RegUser regUser) throws DaoException {
		boolean isLoginFree = true; // default value

		List<Map<String, String>> resultList = performSelectRequest("SELECT * FROM users");

		for (Map<String, String> map : resultList) {

			if (regUser.getLogin().equals(map.get("login"))) {

				isLoginFree = false;
				break;
			}
		}

		if (isLoginFree) {
			createNewClient(regUser);
		}

		return isLoginFree;
	}

	@Override
	public User signIn(String login, String password) throws DaoException {

		User user = checkLoginPassword(login, password);

		return user;
	}

	@Override
	public Set<User> getUsers() throws DaoException {

		Set<User> users = new TreeSet<User>();

		List<Map<String, String>> resultList = performSelectRequest("SELECT name, surname, login, role_name FROM users "
				+ "INNER JOIN users_m2m_user_roles AS m2m ON users.id = m2m.id "
				+ "INNER JOIN user_roles AS roles ON roles.role_id  = m2m.role_id");

		for (Map<String, String> map : resultList) {
			String name = map.get("name");
			String surname = map.get("surname");
			String login = map.get("login");
			String role = map.get("role_name");

			User user = new User(name, surname, login, role);

			users.add(user);
		}

		return users;
	}

	/**
	 * Checks the correspondence between the login and password entered by the
	 * user, with the login and password which are presented in the database
	 * (DB).
	 * 
	 * @param login
	 *            - user login.
	 * @param password
	 *            - user password.
	 * @return <code>User</code>. If input login and password exist in a DB then
	 *         <code>User</code>'s name and surname are not null. Otherwise,
	 *         <code>User</code>'s name and surname are null.
	 * @throws DaoException
	 */
	private User checkLoginPassword(String login, String password) throws DaoException {

		String nameDB = null;
		String surnameDB = null;
		String roleNameDB = null;

		List<Map<String, String>> resultList = performSelectRequest("SELECT * FROM users");

		for (Map<String, String> map : resultList) {
			if (login.equals(map.get("login"))) {
				nameDB = map.get("name");

				if (password.equals(map.get("password"))) {
					surnameDB = map.get("surname");
					roleNameDB = getRoleName(login);
					break;
				}
			}
		}

		User user = new User();
		user.setName(nameDB);
		user.setSurname(surnameDB);
		user.setRoleName(roleNameDB);

		return user;
	}

	/**
	 * Returns the role name for the user with the specified login.
	 * 
	 * @param login
	 *            - user login.
	 * @return user role name.
	 * @throws DaoException
	 */
	private String getRoleName(String login) throws DaoException {

		String roleName = null;

		List<Map<String, String>> resultList = performSelectRequest("SELECT role_name FROM users "
				+ "INNER JOIN users_m2m_user_roles AS m2m ON users.id = m2m.id "
				+ "INNER JOIN user_roles AS roles ON roles.role_id  = m2m.role_id " + "WHERE login = '" + login + "' ");

		for (Map<String, String> map : resultList) {
			roleName = map.get("role_name");
		}

		return roleName;
	}

	/**
	 * Creates an entry for a new client in the database.
	 * 
	 * @param newClient
	 *            - new client.
	 * @throws DaoException
	 */
	private void createNewClient(RegUser newClient) throws DaoException {

		performUpdateRequest("INSERT INTO users (`login`, `password`, `name`, `surname`, `age`, `e-mail`) VALUES "
				+ "('" + newClient.getLogin() + "', '" + newClient.getPassword() + "'," + " '" + newClient.getName()
				+ "', '" + newClient.getSurname() + "'," + " '" + newClient.getAge() + "', '" + newClient.getMail()
				+ "')");

		performUpdateRequest("INSERT INTO users_m2m_user_roles (`id`, `role_id`)"
				+ " SELECT id, '2' FROM users WHERE login = '" + newClient.getLogin() + "'");

	}

}
